#include <doctest/doctest.h>

#include "probes.hpp"

#include <funnel/active_list/disjoint_set.hpp>
#include <funnel/key_view/identity.hpp>
#include <funnel/layer/basic.hpp>
#include <funnel/layer_array/basic.hpp>
#include <funnel/layer_array/indexed.hpp>
#include <funnel/value_location/binary_heap.hpp>

#include <cstddef>
#include <future>
#include <iostream>
#include <list>
#include <memory>
#include <random>
#include <set>
#include <type_traits>
#include <unordered_map>
#include <vector>

using namespace std;

namespace /* unnamed */ {

// Test configurations

using Rng = mt19937_64;
static constexpr Rng::result_type rng_seed{12345678};
static constexpr size_t num_random_experiments{5};

using Value = int;
using KeyView = funnel::key_view::IdentityKeyView<Value>;
using Allocator = allocator<void*>;
using Layer =
    funnel::layer::BasicLayer<Value, Allocator,
                              funnel::active_list::DisjointSetActiveDeque>;
using LayerAllocator = std::allocator_traits<Allocator>::rebind_alloc<Layer>;
using VectorBasicLayerArray =
    funnel::layer_array::BasicLayerArray<Layer, LayerAllocator, vector>;
using ListIndexedLayerArray =
    funnel::layer_array::IndexedLayerArray<Layer, LayerAllocator, list>;

template<class ValueLocation>
struct Test {
  using Id = typename ValueLocation::Id;
  using Index = typename ValueLocation::Index;
  using Probe = funnel::value_location::ValueLocationProbe<ValueLocation>;
  using LayerArray = typename ValueLocation::LayerArray;

  KeyView key_view;
  LayerArray layers;
  multiset<Value> values;
  Rng rng{rng_seed};

  void generateLayers(size_t num_layers, size_t layer_size, size_t num_removals,
                      Value min_value, Value max_step) {
    values.clear();
    layers.clear();
    uniform_int_distribution<Value> gen_value{0, max_step};
    uniform_int_distribution<Index> gen_index{0, layer_size - 1};
    for (size_t i{0}; i < num_layers; ++i) {
      Layer layer;
      Value value{min_value};
      for (size_t j{0}; j < layer_size; ++j) {
        value += gen_value(rng);
        layer.emplaceBack(value);
        values.emplace(value);
      }
      for (size_t j{0}; j < num_removals; ++j) {
        Index index{gen_index(rng)};
        if (layer.isValid(index)) {
          values.erase(values.find(layer[index]));
          layer.erase(index);
        }
      }
      layers.newLayer(std::move(layer));
    }
  }

  ValueLocation createValueLocation() { return ValueLocation(&layers); }

  Value minValue() { return *values.begin(); }

  Value maxValue() { return *(--values.end()); }

  template<class Iterator>
  void check(ValueLocation& value_location, Iterator it) {
    if (value_location.isPastEnd()) {
      REQUIRE_EQ(it, values.end());
      return;
    }
    REQUIRE_EQ(*it, value_location.value());
  }

  static void checkCopies(ValueLocation const& value_location) {
    // Check that copy constructor works.
    ValueLocation value_location_2{value_location};
    Probe::checkEqual(value_location, value_location_2);

    // Check that copy assignment works.
    ValueLocation value_location_3{};
    value_location_3 = value_location;
    Probe::checkEqual(value_location, value_location_3);
  }

  void checkForwardIteration() {
    auto it{values.begin()};
    ValueLocation value_location(&layers);
    value_location.moveToBegin(key_view);
    Index order{value_location.getOrder()};
    while (!value_location.isPastEnd()) {
      checkCopies(value_location);
      REQUIRE_NE(it, values.end());
      CHECK_EQ(*it, value_location.value());
      ++it;
      value_location.moveForward(key_view);

      // Order must increase after moveForward().
      CHECK_LT(order, value_location.getOrder());
      order = value_location.getOrder();
    }
    checkCopies(value_location);
    REQUIRE_EQ(it, values.end());
  }

  void checkBackwardIteration() {
    auto it{values.end()};
    ValueLocation value_location(&layers);
    value_location.moveToEnd(key_view);
    Index order{value_location.getOrder()};
    checkCopies(value_location);
    while (!value_location.isAtBegin()) {
      REQUIRE_NE(it, values.begin());
      --it;
      value_location.moveBackward(key_view);

      // Order must decrease after moveBackward().
      CHECK_GT(order, value_location.getOrder());
      order = value_location.getOrder();

      checkCopies(value_location);
      CHECK_EQ(*it, value_location.value());
    }
    REQUIRE_EQ(it, values.begin());
  }

  void checkLowerBound(Value value) {
    ValueLocation value_location(&layers);
    value_location.moveToLowerBound(value, key_view);
    check(value_location, values.lower_bound(value));
#if TEST_WITH_THREADS
    ValueLocation async_value_location(&layers);
    async_value_location.moveToLowerBound(value, key_view);
    Probe::checkEqual(value_location, async_value_location);
#endif
  }

  void checkUpperBound(Value value) {
    ValueLocation value_location(&layers);
    value_location.moveToUpperBound(value, key_view);
    check(value_location, values.upper_bound(value));
#if TEST_WITH_THREADS
    ValueLocation async_value_location(&layers);
    async_value_location.moveToUpperBound(value, key_view);
    Probe::checkEqual(value_location, async_value_location);
#endif
  }

  void checkValueRange(Value min_value, Value max_value) {
    for (Value value{min_value}; value <= max_value; ++value) {
      checkLowerBound(value);
      checkUpperBound(value);
    }
  }

  void checkLayerIndex(Id id, Index index) {
    if (id >= layers.numLayers()) {
      return;
    }
    if (!layers.layer(id).isValid(index)) {
      return;
    }
    ValueLocation value_location(&layers);
    value_location.moveTo(id, index, key_view);
    REQUIRE_EQ(value_location.getId(), id);
    REQUIRE_EQ(value_location.getIndex(), index);
    REQUIRE_EQ(value_location.value(), layers.layer(id)[index]);

#if TEST_WITH_THREADS
    ValueLocation async_value_location(&layers);
    async_value_location.moveTo(id, index, key_view, std::launch::async);
    Probe::checkEqual(value_location, async_value_location);
#endif
  }

  void checkAllIndices() {
    for (Id id{0}; id < layers.numLayers(); ++id) {
      Layer& layer{layers.layer(id)};
      for (Index index{layer.begin()}; index < layer.end();
           index = layer.next(index)) {
        checkLayerIndex(id, index);
      }
    }
  }

  void checkConsistency() {
    unordered_map<Index, ValueLocation> order_2_value_location;
    ValueLocation value_location(&layers);
    value_location.moveToBegin(key_view);
    REQUIRE(value_location.isAtBegin());

    // Collect all ValueLocations in the map order_2_value_location.
    for (; !value_location.isPastEnd(); value_location.moveForward(key_view)) {
      order_2_value_location[value_location.getOrder()] = value_location;
    }
    // The "end" location must be recorded too.
    order_2_value_location[value_location.getOrder()] = value_location;

    // Check that backward iteration yields the same ValueLocations.
    value_location.moveToEnd(key_view);
    while (!value_location.isAtBegin()) {
      value_location.moveBackward(key_view);
      Index order{value_location.getOrder()};
      REQUIRE_GT(order_2_value_location.count(order), 0);
      Probe::checkEqual(value_location, order_2_value_location[order]);
    }

    // Check that moveToLowerBound yields the same ValueLocations.
    for (Value value{minValue() - 1}; value <= maxValue() + 1; ++value) {
      value_location.moveToLowerBound(value, key_view);
      Index order{value_location.getOrder()};
      REQUIRE_GT(order_2_value_location.count(order), 0);
      Probe::checkEqual(value_location, order_2_value_location[order]);
    }

    // Check that moveToUpperBound yields the same ValueLocations.
    for (Value value{minValue() - 1}; value <= maxValue() + 1; ++value) {
      value_location.moveToUpperBound(value, key_view);
      Index order{value_location.getOrder()};
      REQUIRE_GT(order_2_value_location.count(order), 0);
      Probe::checkEqual(value_location, order_2_value_location[order]);
    }

    // Check that moveTo yields the same ValueLocations.
    for (Id id{0}; id < layers.numLayers(); ++id) {
      Layer& layer{layers.layer(id)};
      for (Index index{layer.begin()}; index < layer.end();
           index = layer.next(index)) {
        value_location.moveTo(id, index, key_view);
        Index order{value_location.getOrder()};
        REQUIRE_GT(order_2_value_location.count(order), 0);
        Probe::checkEqual(value_location, order_2_value_location[order]);
      }
    }
  }

  // For debugging.
  void dump() {
    for (size_t i{0}; i < layers.numLayers(); ++i) {
      Layer& layer{layers.layer(i)};
      for (size_t j{0}; j < layer.end(); ++j) {
        if (layer.isValid(j)) {
          cout << layer[j] << " ";
        } else {
          cout << "? ";
        }
      }
      cout << endl;
    }
    cout << "*****" << endl;
  }
};

using BinaryHeapValueLocationOnVectorBasicLayerArray =
    typename funnel::value_location::BinaryHeapValueLocation<
        VectorBasicLayerArray*>;
using BinaryHeapValueLocationOnListIndexedLayerArray =
    typename funnel::value_location::BinaryHeapValueLocation<
        ListIndexedLayerArray*>;

}  // unnamed namespace

TYPE_TO_STRING(BinaryHeapValueLocationOnVectorBasicLayerArray);
TYPE_TO_STRING(BinaryHeapValueLocationOnListIndexedLayerArray);

TEST_SUITE_BEGIN("value_location");

TEST_CASE_TEMPLATE("value_location -- ", ValueLocation,
                   BinaryHeapValueLocationOnVectorBasicLayerArray,
                   BinaryHeapValueLocationOnListIndexedLayerArray) {
  using Probe = typename Test<ValueLocation>::Probe;
  Test<ValueLocation> test;

  static constexpr size_t num_layers{15};
  static constexpr size_t layer_size{100};
  static constexpr size_t num_removals{30};
  static constexpr Value min_value{-75};
  static constexpr Value max_step{3};

  test.generateLayers(num_layers, layer_size, num_removals, min_value,
                      max_step);

  SUBCASE("check forward iterations") { test.checkForwardIteration(); }

  SUBCASE("check forward iterations") { test.checkBackwardIteration(); }

  SUBCASE("check moveToLowerBound and moveToUpperBound") {
    test.checkValueRange(test.minValue() - 1, test.maxValue() + 1);
  }

  SUBCASE("check moveTo") { test.checkAllIndices(); }

  SUBCASE("check consistency of all ValueLocations") {
    // Check that all ValueLocations are obtainable from iterating from the
    // beginning, and there is a unique ValueLocation for each valid order.
    // (Note that not all consecutive numbers are valid orders.)
    test.checkConsistency();
  }
}

TEST_SUITE_END();
