#include <doctest/doctest.h>

#include "probes.hpp"

#include <funnel/active_list/disjoint_set.hpp>
#include <funnel/key_view/identity.hpp>
#include <funnel/layer/basic.hpp>
#include <funnel/value_location/binary_heap.hpp>

#include <algorithm>
#include <cstddef>
#include <deque>
#include <iostream>
#include <limits>
#include <memory>
#include <random>
#include <set>
#include <vector>

using namespace std;

namespace /* unnamed */ {

using HeapProbe = funnel::value_location::BinaryHeapProbe;

// Test configurations

using Rng = mt19937_64;
static constexpr Rng::result_type rng_seed{12345678};
static constexpr size_t num_random_experiments{5};

using Value = int;
using KeyView = funnel::key_view::IdentityKeyView<Value>;
using Allocator = allocator<size_t>;
using Heap = funnel::value_location::BinaryHeap<Allocator>;
using Id = typename Heap::Id;

}  // unnamed namespace

TEST_SUITE_BEGIN("value_location");

TEST_CASE("BinaryHeap") {
  static constexpr size_t heap_size{1234};
  static constexpr int value_range{1234};

  Rng rng{rng_seed};
  int data[heap_size];
  int min_value{numeric_limits<int>::max()};
  int max_value{numeric_limits<int>::min()};
  for (size_t i{0}; i < heap_size; ++i) {
    int value{uniform_int_distribution<int>(-value_range, value_range)(rng)};
    data[i] = value;
    min_value = min(min_value, value);
    max_value = max(max_value, value);
  }

  auto compare = [&data](Id a, Id b) { return data[a] < data[b]; };

  Heap heap{heap_size};
  heap.build(compare);

  SUBCASE("check that copies are equal") {
    Heap heap_2{heap};
    HeapProbe::checkEqual(heap, heap_2);

    Heap heap_3{heap_size};
    heap_3 = heap;
    HeapProbe::checkEqual(heap, heap_3);

    Heap heap_4{heap_size};
    heap_4.build(compare);
    HeapProbe::checkEqual(heap, heap_4);
  }

  SUBCASE("check reset") {
    Heap heap_2;
    CHECK(heap_2.empty());
    CHECK_EQ(heap_2.size(), 0);

    heap_2.reset(heap_size);
    heap_2.build(compare);
    HeapProbe::checkEqual(heap, heap_2);
  }

  SUBCASE("check minId") { HeapProbe::checkMinId(heap, compare); }

  SUBCASE("check increaseValue") {
    int sorted_data[heap_size];
    copy(data, data + heap_size, sorted_data);
    sort(sorted_data, sorted_data + heap_size);

    for (size_t i{0}; i < heap_size; ++i) {
      Id min_id{heap.minId()};
      CHECK_EQ(data[min_id], sorted_data[i]);
      data[min_id] = max_value + 1;
      heap.increaseValue(min_id, compare);
    }
  }

  SUBCASE("check decreaseValue") {
    for (size_t i{0}; i < heap_size; ++i) {
      int new_value{min_value - static_cast<int>(i) - 1};
      data[i] = new_value;
      heap.decreaseValue(i, compare);
      CHECK_EQ(data[heap.minId()], new_value);
    }
  }

  SUBCASE("random test") {
    multiset<int> sorted(data, data + heap_size);
    for (size_t i{0}; i < num_random_experiments * heap_size; ++i) {
      REQUIRE_EQ(*sorted.begin(), data[heap.minId()]);
      size_t id{uniform_int_distribution<size_t>(0, heap_size - 1)(rng)};
      int old_value{data[id]};
      int new_value{
          uniform_int_distribution<int>(-value_range, value_range)(rng)};
      if (new_value == old_value) {
        continue;
      }
      sorted.erase(sorted.find(old_value));
      sorted.insert(new_value);
      data[id] = new_value;
      if (new_value > old_value) {
        heap.increaseValue(id, compare);
      } else {
        heap.decreaseValue(id, compare);
      }
    }
  }
}

TEST_SUITE_END();

