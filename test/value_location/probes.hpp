#include <doctest/doctest.h>

#include <funnel/value_location/binary_heap.hpp>

#include <iostream>

namespace funnel::value_location {

class BinaryHeapProbe {
public:
  template<class Heap1, class Heap2>
  static void checkEqual(Heap1& a, Heap2& b) {
    REQUIRE_EQ(a.size(), b.size());
    for (typename Heap1::size_type i{0}; i < a.size(); ++i) {
      CHECK_EQ(a.idAt(i), b.idAt(i));
      CHECK_EQ(a.indexOf(i), b.indexOf(i));
    }
  }

  template<class Heap, class Compare>
  static void checkMinId(Heap heap, Compare const& compare) {
    for (typename Heap::Id id{0}; id < heap.size(); ++id) {
      CHECK_FALSE(compare(id, heap.minId()));
    }
  }
};

class BinaryHeapValueLocationProbe {
public:
  // For debugging.
  template<class ValueLocationT>
  static void dump(ValueLocationT&& a) {
    using namespace std;
    using ValueLocation = remove_reference_t<ValueLocationT>;
    using Id = typename ValueLocation::Id;
    using Index = typename ValueLocation::Index;
    using Layer = typename ValueLocation::Layer;
    cout << "id: " << a.getId() << ", index: " << a.getIndex() << "\n";
    cout << "fw_index: ";
    for (Id id{0}; id < a.numLayers(); ++id) {
      Index index{a.id_2_index_pair_[id].fw_index};
      Layer& layer{a.getLayer(id)};
      cout << "[" << index << "]";
      if (layer.isValid(index)) {
        cout << layer[index];
      } else {
        cout << "?";
      }
      cout << " ";
    }
    cout << "\n";
    cout << "bw_index: ";
    for (Id id{0}; id < a.numLayers(); ++id) {
      if (a.id_2_index_pair_[id].bw_dist == 0) {
        cout << "[?]? ";
        continue;
      }
      Index index{a.id_2_index_pair_[id].bwIndex()};
      Layer& layer{a.getLayer(id)};
      cout << "[" << index << "]";
      if (layer.isValid(index)) {
        cout << layer[index];
      } else {
        cout << "?";
      }
      cout << " ";
    }
    cout << endl;
  }

  template<class ValueLocation1, class ValueLocation2>
  static void checkEqual(ValueLocation1& a, ValueLocation2& b) {
    for (typename ValueLocation1::Id i{0}; i < a.numLayers(); ++i) {
      REQUIRE_EQ(a.id_2_index_pair_[i].fw_index,
                 b.id_2_index_pair_[i].fw_index);
      REQUIRE_EQ(a.id_2_index_pair_[i].bw_dist, b.id_2_index_pair_[i].bw_dist);
    }
  }
};

template<class ValueLocationT>
class ValueLocationProbe;

template<>
class ValueLocationProbe<void> {
public:
  template<class ValueLocation1, class ValueLocation2>
  static void checkEqual(ValueLocation1&& a, ValueLocation2&& b) {
    REQUIRE_EQ(a, b);
    REQUIRE_EQ(a.numLayers(), b.numLayers());
    REQUIRE_EQ(a.getLayerArray(), b.getLayerArray());
    REQUIRE_EQ(a.getOrder(), b.getOrder());
    REQUIRE_EQ(a.getId(), b.getId());
    REQUIRE_EQ(a.getIndex(), b.getIndex());
  }
};

// Probe for BinaryHeapValueLocation.
template<class LayerArrayPointer, class Allocator>
class ValueLocationProbe<
    BinaryHeapValueLocation<LayerArrayPointer, Allocator>> {
public:
  template<class ValueLocation1, class ValueLocation2>
  static void checkEqual(ValueLocation1&& a, ValueLocation2&& b) {
    ValueLocationProbe<void>::checkEqual(a, b);
    BinaryHeapValueLocationProbe::checkEqual(a, b);
  }
};

// Default probe.
template<class ValueLocationT>
class ValueLocationProbe : public ValueLocationProbe<void> {
public:
  using Super = ValueLocationProbe<void>;
  template<class ValueLocation1, class ValueLocation2>
  static void checkEqual(ValueLocation1&& a, ValueLocation2&& b) {
    Super::checkEqual(a, b);
  }
};

}  // namespace funnel::value_location

