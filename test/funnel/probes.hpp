#pragma once

#include <doctest/doctest.h>

#include <funnel/funnel/list.hpp>

namespace funnel::funnel {

template<class Layer>
class Probe;

template<>
class Probe<void> {
public:
  template<class Funnel1, class Funnel2>
  static constexpr void checkEqual(Funnel1& funnel_1, Funnel2& funnel_2) {
    CHECK_EQ(funnel_1.size(), funnel_2.size());
  }
};

template<class Layer>
class Probe : public Probe<void> {
public:
  using Super = Probe<void>;
  using Super::checkEqual;
};

}  // namespace funnel::funnel
