#include <doctest/doctest.h>

#include "probes.hpp"

#include <funnel/active_list/disjoint_set.hpp>
#include <funnel/key_view/pair.hpp>
#include <funnel/layer/basic.hpp>

#include <functional>
#include <iostream>
#include <iterator>
#include <limits>
#include <map>
#include <memory>
#include <random>

using namespace std;

namespace /* unnamed */ {

// Test configurations

using Rng = mt19937_64;
static constexpr Rng::result_type rng_seed{12345678};

using Key = int;
using Mapped = int;
using KeyView = funnel::key_view::PairKeyView<Key, Mapped>;
using Value = KeyView::Value;
using Allocator = allocator<void*>;

template<class Layer>
struct TestLayer {
  using Index = typename Layer::Index;
  using Map = multimap<Key, Mapped>;
  using This = TestLayer<Layer>;
  using Probe = funnel::layer::Probe<Layer>;
  Layer layer;
  static constexpr KeyView key_view{};
  Map map;
  Key min_key{0};
  Key max_key{0};
  static constexpr bool is_two_sided{Layer::is_two_sided};
  Rng rng{rng_seed};

  TestLayer() = default;
  TestLayer(Layer&& new_layer) : layer{std::move(new_layer)} {
    updateFromLayer();
  }
  TestLayer(This const&) = default;
  TestLayer(This&&) = default;
  This& operator=(This const&) = default;
  This& operator=(This&&) = default;

  void clear() {
    layer.clear();
    map.clear();
    min_key = 0;
    max_key = 0;
  }

  constexpr size_t size() noexcept {
    CHECK_EQ(layer.size(), map.size());
    return layer.size();
  }

  constexpr Key const& keyAt(Index index) noexcept {
    return key_view.key(layer[index]);
  }

  constexpr void updateFromLayer() {
    if (layer.size() == 0) {
      clear();
      return;
    }
    map.clear();
    for (Index i{layer.begin()}; i < layer.end(); i = layer.next(i)) {
      map.emplace(key_view.key(layer[i]), key_view.mapped(layer[i]));
    }
    min_key = key_view.key(*map.begin());
    max_key = key_view.key(*map.rbegin());
  }

  void checkEqual(TestLayer& other) {
    Probe::checkEqual(layer, other.layer, key_view);
  }

  template<bool check = true>
  void checkKey(Index index, Map::iterator it) {
    if constexpr (check) {
      if (index == layer.end()) {
        REQUIRE_EQ(it, map.end());
        return;
      }
      REQUIRE_NE(it, map.end());
      REQUIRE_EQ(it->first, key_view.key(layer[index]));
    }
  }

  template<bool check = true>
  void checkAllKeys() {
    if constexpr (check) {
      REQUIRE_EQ(layer.size(), map.size());
      auto it{map.begin()};
      for (Index index{layer.begin()}; index < layer.end();
           index = layer.next(index)) {
        checkKey<check>(index, it);
        ++it;
      }
      if (!layer.empty()) {
        REQUIRE_FALSE(map.empty());
        checkKey(layer.frontIndex(), map.begin());
        checkKey(layer.backIndex(), prev(map.end()));
      }

      it = map.end();
      for (Index index{layer.end()}; index > layer.begin();) {
        --index;
        --it;
        checkKey<check>(index, it);
      }
    }
  }

  template<bool check = true>
  void checkValue(Index index, Map::iterator it) {
    if constexpr (check) {
      if (index == layer.end()) {
        REQUIRE_EQ(it, map.end());
        return;
      }
      REQUIRE_NE(it, map.end());
      REQUIRE_EQ(it->first, key_view.key(layer[index]));
      REQUIRE_EQ(it->second, key_view.mapped(layer[index]));
    }
  }

  template<bool check = true>
  void checkAllValues() {
    if constexpr (check) {
      REQUIRE_EQ(layer.size(), map.size());
      auto it{map.begin()};
      for (Index index{layer.begin()}; index < layer.end();
           index = layer.next(index)) {
        checkValue(index, it);
        ++it;
      }
      if (!layer.empty()) {
        REQUIRE_FALSE(map.empty());
        checkValue(layer.frontIndex(), map.begin());
        checkValue(layer.backIndex(), prev(map.end()));
      }
    }
  }

  template<bool check = true>
  void emplaceBack(Key const& step) {
    Mapped mapped{uniform_int_distribution<Mapped>()(rng)};
    max_key += step;
    checkKey(layer.emplaceBack(max_key, mapped), map.emplace(max_key, mapped));
    checkAllKeys<check>();
  }

  template<bool check = true>
  void emplaceBack(Key const& min_step, Key const& max_step) {
    emplaceBack<check>(uniform_int_distribution<Key>(min_step, max_step)(rng));
  }

  template<bool check = true>
  void emplaceFront(Key const& step) {
    if constexpr (is_two_sided) {
      Mapped mapped{uniform_int_distribution<Mapped>()(rng)};
      min_key -= step;
      checkKey(layer.emplaceFront(min_key, mapped),
               map.emplace(min_key, mapped));
      checkAllKeys<check>();
    } else {
      REQUIRE(false);
    }
  }

  template<bool check = true>
  void emplaceFront(Key const& min_step, Key const& max_step) {
    emplaceFront<check>(uniform_int_distribution<Key>(min_step, max_step)(rng));
  }

  template<bool check = true>
  void emplace(Key const& step) {
    if constexpr (is_two_sided) {
      if (bernoulli_distribution()(rng)) {
        emplaceBack<check>(step);
      } else {
        emplaceFront<check>(step);
      }
    } else {
      emplaceBack<check>(step);
    }
  }

  template<bool check = true>
  void emplace(Key const& min_step, Key const& max_step) {
    emplace<check>(uniform_int_distribution<Key>(min_step, max_step)(rng));
  }

  template<bool check = true>
  Index lowerBound(Key const& key) {
    Index index{layer.lowerBound(key, key_view)};
    auto it{map.lower_bound(key)};
    checkKey<check>(index, it);
    return index;
  }

  template<bool check = true>
  Index upperBound(Key const& key) {
    Index index{layer.upperBound(key, key_view)};
    auto it{map.upper_bound(key)};
    checkKey<check>(index, it);
    return index;
  }

  template<bool check = true>
  Index find(Key const& key) {
    Index index{layer.find(key, key_view)};
    auto it{map.find(key)};
    checkKey<check>(index, it);
    return index;
  }

  template<bool check = true>
  bool contains(Key const& key) {
    bool contains_key{layer.contains(key, key_view)};
    if constexpr (check) {
      REQUIRE(layer.contains(key, key_view) == map.contains(key));
    }
    return contains_key;
  }

  template<bool check = true>
  pair<Index, Index> equalRange(Key const& key) {
    pair<Index, Index> index_pair{layer.equalRange(key, key_view)};
    pair<Map::iterator, Map::iterator> it_pair{map.equal_range(key)};
    checkKey<check>(index_pair.first, it_pair.first);
    checkKey<check>(index_pair.second, it_pair.second);
    return index_pair;
  }

  template<bool check = true>
  size_t count(Key const& key) {
    size_t c{layer.count(key, key_view)};
    if constexpr (check) {
      REQUIRE_EQ(c, map.count(key));
    }
    return c;
  }

  template<bool check = false>
  void fill(size_t count, Key const& min_step, Key const& max_step) {
    for (size_t i{0}; i < count; ++i) {
      emplace<check>(min_step, max_step);
    }
  }

  template<bool check = true>
  void checkSearchResults() {
    if constexpr (check) {
      for (Key key{min_key - 1}; key <= max_key + 1; ++key) {
        lowerBound(key);
        upperBound(key);
        find(key);
        equalRange(key);
        count(key);
        contains(key);
      }
    }
  }

  template<bool check = true>
  void checkCopies() {
    if constexpr (check) {
      This other{*this};
      checkEqual(other);

      other.clear();
      other = *this;
      checkEqual(other);
    }
  }

  template<bool check = true>
  Index eraseKey(Key const& key) {
    pair<Index, Index> index_pair{layer.equalRange(key, key_view)};
    pair<Map::iterator, Map::iterator> it_pair{map.equal_range(key)};
    Index next_index{layer.erase(index_pair.first, index_pair.second)};
    Map::iterator next_it{map.erase(it_pair.first, it_pair.second)};
    checkKey<check>(next_index, next_it);
    return next_index;
  }

  Key randKey(Index front_offset = 0, Index back_offset = 0) {
    REQUIRE_GT(map.size(), front_offset + back_offset);
    size_t order{uniform_int_distribution<size_t>(
        front_offset, map.size() - 1 - back_offset)(rng)};
    return next(map.begin(), order)->first;
  }

  template<bool check = true>
  void eraseRandomKeys(size_t count = numeric_limits<size_t>::max()) {
    for (size_t i{0}; i < count; ++i) {
      if (map.empty()) {
        break;
      }
      eraseKey<check>(randKey());
      checkSearchResults<check>();
    }
  }

  template<class... Indices>
  void optimize(Indices&... tracked_indices) {
    layer.optimize(tracked_indices...);
    updateFromLayer();
  }

  template<class TrackedIndices, class TrackedOtherIndices>
  This mergeWith(This& other, TrackedIndices tracked_indices,
                 TrackedOtherIndices tracked_other_indices) {
    This output{layer.mergeWith(other.layer, tracked_indices,
                                tracked_other_indices, key_view)};
    clear();
    other.clear();
    return output;
  }

  This mergeWith(This& other) {
    return mergeWith(other, funnel::layer::trackIndices(),
                     funnel::layer::trackIndices());
  }
};

using BasicDequeLayer =
    funnel::layer::BasicLayer<Value, Allocator,
                              funnel::active_list::DisjointSetActiveDeque>;
using BasicVectorLayer =
    funnel::layer::BasicLayer<Value, Allocator,
                              funnel::active_list::DisjointSetActiveVector>;

}  // unnamed namespace

TYPE_TO_STRING(BasicDequeLayer);
TYPE_TO_STRING(BasicVectorLayer);

TEST_CASE_TEMPLATE("layer -- ", Layer, BasicDequeLayer, BasicVectorLayer) {
  using Test = TestLayer<Layer>;
  using Index = typename Test::Index;
  using Map = typename Test::Map;

  static constexpr size_t test_layer_size{86};
  static constexpr size_t num_removals{24};
  static constexpr size_t num_random_experiments{5};

  Test test;

  SUBCASE("empty") {
    SUBCASE("copy") { test.checkCopies(); }
    SUBCASE("search") { test.checkSearchResults(); }
    SUBCASE("optimize") {
      Index i{test.layer.end()};
      test.optimize(i);
      CHECK_EQ(i, test.layer.end());
    }
  }

  SUBCASE("fill") {
    for (size_t i{0}; i < test_layer_size; ++i) {
      test.emplace(1);
    }
  }

  SUBCASE("fill, then check") {
    for (size_t i{0}; i < test_layer_size; ++i) {
      test.template emplace<false>(1);
    }

    SUBCASE("consistency") { test.checkAllValues(); }
    SUBCASE("copy") { test.checkCopies(); }
    SUBCASE("search") { test.checkSearchResults(); }
    SUBCASE("optimize") {
      Index indices[4];
      indices[0] = 0;
      indices[1] = test_layer_size / 2;
      indices[2] = test_layer_size - 1;
      indices[3] = test_layer_size;
      test.optimize(indices[0], indices[1], indices[2], indices[3]);
      CHECK_EQ(indices[0], 0);
      CHECK_EQ(indices[1], test_layer_size / 2);
      CHECK_EQ(indices[2], test_layer_size - 1);
      CHECK_EQ(indices[3], test_layer_size);
      CHECK_EQ(test.layer.size(), test_layer_size);
    }
  }

  SUBCASE("fill with gaps and duplicates") {
    test.fill(test_layer_size, 0, 3);

    SUBCASE("consistency") { test.checkAllKeys(); }
    SUBCASE("copy") { test.checkCopies(); }
    SUBCASE("search") { test.checkSearchResults(); }
  }

  SUBCASE("fill and remove") {
    for (size_t i{0}; i < num_random_experiments; ++i) {
      test.template fill<false>(test_layer_size, 0, 3);
      test.eraseRandomKeys();
      test.template fill<false>(test_layer_size, 0, 3);
      test.template eraseRandomKeys<false>(test_layer_size / 2);

      test.checkCopies();

      test.eraseRandomKeys();
      test.clear();
    }
  }

  SUBCASE("optimize and track") {
    for (size_t i{0}; i < num_random_experiments; ++i) {
      test.template fill<false>(test_layer_size, 1, 4);
      test.template eraseRandomKeys<false>(test_layer_size / 2);

      {
        Index indices[4];
        indices[0] = test.layer.begin();
        indices[1] = test.find(test.randKey(1, 1));
        indices[2] = test.layer.backIndex();
        indices[3] = test.layer.end();
        Key keys[3];
        for (size_t j{0}; j < 3; ++j) {
          keys[j] = test.keyAt(indices[j]);
        }

        size_t old_size{test.layer.size()};
        test.optimize(indices[0], indices[1], indices[2], indices[3]);
        CHECK_EQ(old_size, test.layer.size());

        for (size_t j{0}; j < 3; ++j) {
          CHECK_EQ(keys[j], test.keyAt(indices[j]));
        }
        CHECK_EQ(indices[3], test.layer.end());
      }

      test.clear();
    }
  }
}

TEST_CASE_TEMPLATE("layer::merger -- ", Layer, BasicDequeLayer,
                   BasicVectorLayer) {
  using Test = TestLayer<Layer>;
  using Index = typename Test::Index;
  using Map = typename Test::Map;

  static constexpr size_t test_layer_size{246};
  static constexpr size_t num_removals{71};
  static constexpr size_t num_random_experiments{35};
  static constexpr Key max_step{3};

  Test test_1;
  Test test_2;
  test_2.rng.seed(rng_seed + 1);

  SUBCASE("merge empty layers") {
    Test merged{test_1.mergeWith(test_2)};
    merged.checkAllKeys();

    // Make empty layers by adding and remove all elements.
    for (size_t i{0}; i < num_random_experiments; ++i) {
      test_1.clear();
      test_1.template fill<false>(test_layer_size, 0, max_step);
      test_1.template eraseRandomKeys<false>();
      test_2.clear();
      test_2.template fill<false>(test_layer_size, 0, max_step);
      test_2.template eraseRandomKeys<false>();
      merged = test_1.mergeWith(test_2);
      merged.checkAllKeys();
      CHECK_EQ(merged.size(), 0);
    }
  }

  SUBCASE("merge empty first layer") {
    test_2.template fill<false>(test_layer_size, 0, max_step);
    test_2.template eraseRandomKeys<false>(num_removals);
    Test old_layer{test_2};
    Test merged{test_1.mergeWith(test_2)};
    merged.checkAllKeys();
    merged.checkEqual(old_layer);

    // Make empty layers by adding and remove all elements.
    for (size_t i{0}; i < num_random_experiments; ++i) {
      test_1.clear();
      test_1.template fill<false>(test_layer_size, 0, max_step);
      test_1.template eraseRandomKeys<false>();
      test_2.clear();
      test_2.template fill<false>(test_layer_size, 0, max_step);
      test_2.template eraseRandomKeys<false>(num_removals);

      old_layer = test_2;
      merged = test_1.mergeWith(test_2);
      merged.checkAllKeys();
      merged.checkEqual(old_layer);
    }
  }

  SUBCASE("merge empty second layer") {
    test_1.template fill<false>(test_layer_size, 0, max_step);
    test_1.template eraseRandomKeys<false>(num_removals);
    Test old_layer{test_1};
    Test merged{test_1.mergeWith(test_2)};
    merged.checkAllKeys();
    merged.checkEqual(old_layer);

    // Make empty layers by adding and remove all elements.
    for (size_t i{0}; i < num_random_experiments; ++i) {
      test_1.clear();
      test_1.template fill<false>(test_layer_size, 0, max_step);
      test_1.template eraseRandomKeys<false>(num_removals);
      test_2.clear();
      test_2.template fill<false>(test_layer_size, 0, max_step);
      test_2.template eraseRandomKeys<false>();

      old_layer = test_1;
      merged = test_1.mergeWith(test_2);
      merged.checkAllKeys();
      merged.checkEqual(old_layer);
    }
  }

  SUBCASE("merge non-empty") {
    for (size_t i{0}; i < num_random_experiments; ++i) {
      test_1.clear();
      test_1.template fill<false>(test_layer_size, 0, max_step);
      test_1.template eraseRandomKeys<false>(num_removals);
      test_2.clear();
      test_2.template fill<false>(test_layer_size, 0, max_step);
      test_2.template eraseRandomKeys<false>(num_removals);

      Map combined_map;
      combined_map.insert(test_1.map.begin(), test_1.map.end());
      combined_map.insert(test_2.map.begin(), test_2.map.end());

      Test merged{test_1.mergeWith(test_2)};
      merged.checkAllKeys();

      auto combined_it{combined_map.begin()};
      auto merged_it{merged.map.begin()};
      for (; combined_it != combined_map.end(); ++combined_it, ++merged_it) {
        REQUIRE_NE(merged_it, merged.map.end());
        CHECK_EQ(combined_it->first, merged_it->first);
      }
      REQUIRE_EQ(merged_it, merged.map.end());
    }
  }

  SUBCASE("merge and track") {
    for (size_t i{0}; i < num_random_experiments; ++i) {
      test_1.clear();
      test_1.template fill<false>(test_layer_size, 1, max_step + 1);
      test_1.template eraseRandomKeys<false>(num_removals);
      test_2.clear();
      test_2.template fill<false>(test_layer_size, 1, max_step + 1);
      test_2.template eraseRandomKeys<false>(num_removals);

      Index indices_1[4];
      indices_1[0] = test_1.layer.begin();
      indices_1[1] = test_1.find(test_1.randKey(1, 1));
      indices_1[2] = test_1.layer.backIndex();
      indices_1[3] = test_1.layer.end();

      Index indices_2[4];
      indices_2[0] = test_2.layer.begin();
      indices_2[1] = test_2.find(test_2.randKey(1, 1));
      indices_2[2] = test_2.layer.backIndex();
      indices_2[3] = test_2.layer.end();

      Key keys_1[3];
      Key keys_2[3];
      for (size_t j{0}; j < 3; ++j) {
        keys_1[j] = test_1.keyAt(indices_1[j]);
        keys_2[j] = test_2.keyAt(indices_2[j]);
      }

      using funnel::layer::trackIndices;

      Test merged{test_1.mergeWith(
          test_2,
          trackIndices(indices_1[0], indices_1[1], indices_1[2], indices_1[3]),
          trackIndices(indices_2[0], indices_2[1], indices_2[2],
                       indices_2[3]))};

      for (size_t j{0}; j < 3; ++j) {
        CHECK_EQ(keys_1[j], merged.keyAt(indices_1[j]));
        CHECK_EQ(keys_2[j], merged.keyAt(indices_2[j]));
      }
      CHECK_EQ(indices_1[3], merged.layer.end());
      CHECK_EQ(indices_2[3], merged.layer.end());
    }
  }
}
