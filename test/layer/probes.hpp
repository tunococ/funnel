#pragma once

#include <doctest/doctest.h>

namespace funnel::layer {

// Probe

class BasicLayerProbe {
public:
};


template<class Layer>
class Probe;

template<>
class Probe<void> {
public:
  template<class Layer1, class Layer2, class KeyView>
  static constexpr void checkEqual(Layer1& layer_1, Layer2& layer_2,
      KeyView const& key_view) {
    CHECK_EQ(layer_1.size(), layer_2.size());
    CHECK_EQ(layer_1.compare(layer_2, key_view), 0);
  }
};

template<class Layer>
class Probe : public Probe<void> {
public:
  using Super = Probe<void>;
  using Super::checkEqual;
};

} // namespace funnel::layer

