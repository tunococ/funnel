#include <doctest/doctest.h>

#include <funnel/active_list/disjoint_set.hpp>

#include <iostream>

namespace funnel::active_list {

template<class ActiveList>
class Probe;

template<>
class Probe<void> {
public:
  template<class ActiveList>
  static constexpr void check(ActiveList& active_list) {
    using Index = typename ActiveList::Index;
    Index index{0};
    Index active_index{active_list.beginActiveIndex()};
    Index active_count{0};
    for (Index index{active_list.beginActiveIndex()};
         index < active_list.endActiveIndex(); ++index) {
      if (active_list.isActive(index)) {
        ++active_count;
        REQUIRE_EQ(index, active_index);
        active_index = active_list.nextActiveIndex(active_index);
        continue;
      }
      REQUIRE_LT(index, active_index);
    }
    REQUIRE_EQ(active_count, active_list.numActive());
  }

  template<class ActiveList1, class ActiveList2>
  static constexpr void checkEqual(ActiveList1& a, ActiveList2& b) {
    using Index = typename ActiveList1::Index;
    CHECK_EQ(a.numActive(), b.numActive());
    CHECK_EQ(a.capacity(), b.capacity());
    for (Index i{0}; i < a.capacity(); ++i) {
      if (!a.isActive(i)) {
        CHECK(!b.isActive(i));
        continue;
      }
      CHECK(b.isActive(i));
      if constexpr (ActiveList1::entries_have_values &&
                    ActiveList2::entries_have_values) {
        CHECK_EQ(a.valueAt(i), b.valueAt(i));
      }
    }
  }
};

template<class AL>
class Probe : Probe<void> {
public:
  using Super = Probe<void>;

  template<class ActiveList>
  static constexpr void check(ActiveList& active_list) {
    Super::check(active_list);
  }

  template<class ActiveList1, class ActiveList2>
  static constexpr void checkEqual(ActiveList1& a, ActiveList2& b) {
    Super::checkEqual(a, b);
  }
};

class DisjointSetActiveListProbe : Probe<void> {
public:
  using Super = Probe<void>;

  template<class ActiveList1, class ActiveList2>
  static constexpr void checkEqual(ActiveList1& a, ActiveList2& b) {
    Super::checkEqual(a, b);
  }

  template<class ActiveList>
  static constexpr void check(ActiveList& a) {
    Super::check(a);
  }
};

}  // namespace funnel::active_list

