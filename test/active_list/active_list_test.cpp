#include <doctest/doctest.h>

#include "probes.hpp"

#include <funnel/active_list/disjoint_set.hpp>
#include <funnel/tracker/index.hpp>

#include <cstddef>
#include <deque>
#include <functional>
#include <iostream>
#include <iterator>
#include <random>
#include <set>
#include <utility>
#include <vector>

using namespace std;

namespace /* unnamed */ {

using namespace funnel::active_list;

// Test configurations

using Rng = mt19937_64;
static constexpr Rng::result_type rng_seed{12345678};

static constexpr size_t num_random_experiments{5};

static constexpr size_t test_list_size{101};  // Should be >= 8

/**
 *  @brief
 *  Convenience class for testing `ActiveList`s without values and with values
 *  that are multiples of (static) indices.
 *
 *  This class contains an `active_list` and an `index_set` containing indices
 *  associated to entries in the `active_list`. `index_set` helps with checking
 *  correctness of `active_list`'s operations.
 *
 *  An `active_list` may be one-sided or two-sided based on its underlying data
 *  structure. For example, if the underlying data structure is `std::vector`,
 *  it will be one-sided. If the underlying data structure is `std::deque`,
 *  it will be two-sided.
 *
 *  If a list is one-sided, every entry has a fixed index associated to it.
 *  If a list is two-sided, inserting an entry to the front of the list will
 *  move indices associated with all existing entries up by 1.
 *  To keep track of indices efficiently, we do not change values in
 *  `index_set`. Instead, we keep count of the number of insertions of each
 *  type: front and back. When an entry is added to the back, we assign
 *  `num_back_pushes` as its static index. When an entry is added to the front,
 *  we assign `-num_front_pushes - 1` as its static index.
 *  (The first insertion is always counted as an insertion to the back.)
 *  `index_set` stores static indices of all active entries.
 */
template<class ActiveList>
struct Test {
  using Probe = funnel::active_list::Probe<ActiveList>;

  using Value = typename ActiveList::Value;
  using Index = typename ActiveList::Index;
  using SignedIndex = std::make_signed_t<Index>;
  static constexpr bool entries_have_values{ActiveList::entries_have_values};
  static constexpr bool is_two_sided{ActiveList::is_two_sided};

  ActiveList active_list;
  Index num_back_pushes{0};
  Index num_front_pushes{0};
  Index num_deactivated{0};
  set<SignedIndex> index_set;
  Rng rng{rng_seed};

  constexpr void clear() {
    active_list.clear();
    index_set.clear();
    num_back_pushes = 0;
    num_front_pushes = 0;
    num_deactivated = 0;
  }

  // Basic building blocks.

  constexpr size_t numActive() const noexcept {
    return active_list.numActive();
  }

  template<bool check = true>
  constexpr void emplaceFront() {
    if (num_back_pushes == 0) {
      emplaceBack<check>();
      return;
    }
    ++num_front_pushes;
    if constexpr (entries_have_values) {
      active_list.emplaceFront(-static_cast<SignedIndex>(num_front_pushes));
    } else {
      active_list.emplaceFront();
    }
    index_set.emplace(-static_cast<SignedIndex>(num_front_pushes));
    doChecks<check>();
  }

  template<bool check = true>
  constexpr void emplaceBack() {
    if constexpr (entries_have_values) {
      active_list.emplaceBack(num_back_pushes);
    } else {
      active_list.emplaceBack();
    }
    index_set.emplace(num_back_pushes);
    ++num_back_pushes;
    doChecks<check>();
  }

  template<bool check = true>
  constexpr void deactivate(Index index) {
    REQUIRE(active_list.entryAt(index).isActive());
    active_list.deactivate(index);
    index_set.erase(static_cast<SignedIndex>(index - num_front_pushes));
    ++num_deactivated;
    doChecks<check>();
  }

  template<bool check = true, typename V>
  constexpr Index lowerBoundActiveIndex(V const& value) {
    Index index{active_list.lowerBoundActiveIndex(value)};
    auto it{index_set.lower_bound(value)};
    checkValue<check>(index, it);
    return index;
  }

  template<bool check = true, typename V>
  constexpr Index upperBoundActiveIndex(V const& value) {
    Index index{active_list.upperBoundActiveIndex(value)};
    auto it{index_set.upper_bound(value)};
    checkValue<check>(index, it);
    return index;
  }

  template<bool check = true, typename V>
  constexpr Index findActiveIndex(V const& value) {
    Index index{active_list.findActiveIndex(value)};
    auto it{index_set.find(value)};
    checkValue<check>(index, it);
    return index;
  }

  template<bool check = true, typename V>
  constexpr pair<Index, Index> equalRangeActiveIndices(V const& value) {
    pair<Index, Index> index_pair{active_list.equalRangeActiveIndices(value)};
    auto it_pair{index_set.equal_range(value)};
    checkValue<check>(index_pair.first, it_pair.first);
    checkValue<check>(index_pair.second, it_pair.second);
    return index_pair;
  }

protected:
  template<bool check = true, class Iterator>
  constexpr void checkValue(Index index, Iterator it)
      FUNNEL_REQUIRES(entries_have_values) {
    if constexpr (entries_have_values) {
      if constexpr (check) {
        if (index == active_list.endActiveIndex()) {
          REQUIRE_EQ(it, index_set.end());
        } else {
          REQUIRE(active_list.entryAt(index).isActive());
          REQUIRE_NE(it, index_set.end());
          REQUIRE_EQ(*it, static_cast<SignedIndex>(active_list.valueAt(index)));
        }
      }
    } else {
      FAIL("");
      throw;
    }
  }

  constexpr void checkWithIndexSet() {
    // Iterate forward.
    REQUIRE_EQ(active_list.numActive(), index_set.size());
    auto it{index_set.begin()};
    for (Index index{active_list.beginActiveIndex()};
         index < active_list.endActiveIndex();
         index = active_list.nextActiveIndex(index)) {
      REQUIRE_NE(it, index_set.end());
      REQUIRE_EQ(static_cast<Index>(*it + num_front_pushes), index);
      if constexpr (entries_have_values) {
        REQUIRE_EQ(*it, static_cast<SignedIndex>(active_list.valueAt(index)));
      }
      ++it;
    }
    REQUIRE_EQ(it, index_set.end());

    // Iterate backward.
    it = index_set.end();
    for (Index index{active_list.endActiveIndex()};
         index > active_list.beginActiveIndex();) {
      REQUIRE_NE(it, index_set.begin());
      index = active_list.prevActiveIndex(index);
      --it;
      REQUIRE_EQ(static_cast<Index>(*it + num_front_pushes), index);
      if constexpr (entries_have_values) {
        REQUIRE_EQ(*it, static_cast<SignedIndex>(active_list.valueAt(index)));
      }
    }
    REQUIRE_EQ(it, index_set.begin());

    // Check front and back active indices.
    if (active_list.numActive() > 0) {
      REQUIRE_GT(index_set.size(), 0);
      REQUIRE_EQ(static_cast<Index>(*index_set.begin() + num_front_pushes),
                 active_list.frontActiveIndex());
      REQUIRE_EQ(static_cast<Index>(*index_set.rbegin() + num_front_pushes),
                 active_list.backActiveIndex());
    }
  }

public:
  template<bool check = true>
  constexpr void doChecks() {
    if constexpr (check) {
      Probe::check(active_list);
      checkWithIndexSet();
    }
  }

  // Convenience functions for testing.

  template<bool check = true>
  constexpr void fillBack(size_t reps) {
    for (size_t i{0}; i < reps; ++i) {
      emplaceBack<check>();
    }
  }

  template<bool check = true>
  constexpr void fillFront(size_t reps) {
    for (size_t i{0}; i < reps; ++i) {
      emplaceFront<check>();
    }
  }

  template<bool check = true>
  void fillRandom(size_t reps) {
    if constexpr (is_two_sided) {
      bernoulli_distribution gen_bool{};
      for (size_t i{0}; i < reps; ++i) {
        if (gen_bool(rng)) {
          emplaceFront<check>();
        } else {
          emplaceBack<check>();
        }
      }
    } else {
      fillBack<check>(reps);
    }
  }

  template<bool check = true>
  constexpr void deactivateFront(size_t reps = 1) {
    for (size_t i{0}; i < reps; ++i) {
      REQUIRE_GT(active_list.numActive(), 0);
      deactivate<check>(active_list.beginActiveIndex());
    }
  }

  template<bool check = true>
  constexpr void deactivateBack(size_t reps = 1) {
    for (size_t i{0}; i < reps; ++i) {
      REQUIRE_GT(active_list.numActive(), 0);
      deactivate<check>(
          active_list.prevActiveIndex(active_list.endActiveIndex()));
    }
  }

  template<bool check = true>
  constexpr void deactivateRange(Index from, Index to) {
    REQUIRE_GE(from, 0);
    REQUIRE_LE(to, active_list.endActiveIndex());
    for (Index i{from}; i < to; ++i) {
      if (active_list.entryAt(i).isActive()) {
        deactivate<check>(i);
      }
    }
  }

  template<bool check = true>
  constexpr void deactivateAll() {
    deactivateRange<check>(0, active_list.endActiveIndex());
  }

  template<bool check = true>
  void deactivateRandom(size_t reps = 1) {
    for (size_t i{0}; i < reps; ++i) {
      REQUIRE_GT(index_set.size(), 0);
      uniform_int_distribution<size_t> gen_index_in_set(0,
                                                        index_set.size() - 1);
      Index index{num_front_pushes +
                  *(next(index_set.begin(), gen_index_in_set(rng)))};
      deactivate<check>(index);
    }
  }

  void randomizedOccupationTest(size_t reps) {
    uniform_int_distribution<size_t> gen_index{0, test_list_size - 1};
    for (size_t i{0}; i < reps; ++i) {
      if ((gen_index(rng) > num_front_pushes + num_back_pushes) ||
          numActive() == 0) {
        fillRandom(1);
      } else {
        deactivateRandom(1);
      }
    }
  }

  template<bool track = true>
  void optimize() {
    std::vector<Index> active_indices;
    active_indices.reserve(active_list.numActive());

    Index mid_index{active_list.beginActiveIndex()};
    Index j{0};
    for (Index i{active_list.beginActiveIndex()};
         i < active_list.endActiveIndex(); i = active_list.nextActiveIndex(i)) {
      if constexpr (track) {
        // Define mid_index as the "median" among active indices.
        if (j == active_list.numActive() / 2) {
          mid_index = i;
        }
      }
      active_indices.emplace_back(i);
      ++j;
    }
    CHECK_EQ(active_indices.size(), active_list.numActive());

    if constexpr (track) {
      if (active_list.numActive() > 3) {
        Index front_index{active_list.beginActiveIndex()};
        Index back_index{active_list.numActive() > 0
                             ? active_list.backActiveIndex()
                             : front_index};
        Index end_index{active_list.endIndex()};

        // Track 4 indices: front, mid, back, and end.
        active_list.optimize(funnel::tracker::trackIndices(
            front_index, mid_index, back_index, end_index));

        CHECK_EQ(active_list.capacity(), active_list.numActive());
        CHECK_EQ(active_indices.size(), active_list.numActive());

        CHECK_EQ(front_index, 0);
        CHECK_EQ(back_index, active_list.capacity() - 1);
        CHECK_EQ(mid_index, index_set.size() / 2);
        CHECK_EQ(end_index, active_list.endIndex());
      } else {
        active_list.optimize();
      }
    } else {
      active_list.optimize();
    }

    if constexpr (entries_have_values) {
      CHECK_EQ(index_set.size(), active_list.numActive());
      auto it{index_set.begin()};
      for (Index i{active_list.beginActiveIndex()};
           i < active_list.endActiveIndex();
           i = active_list.nextActiveIndex(i)) {
        CHECK_EQ(*it, active_list.valueAt(i));
        ++it;
      }
    }
  }
};

using Allocator = std::allocator<void*>;
using DisjointSetActiveVoidVector = DisjointSetActiveVector<void, Allocator>;
using DisjointSetActiveVoidDeque = DisjointSetActiveDeque<void, Allocator>;
using DisjointSetActiveIntVector = DisjointSetActiveVector<int, Allocator>;
using DisjointSetActiveIntDeque = DisjointSetActiveDeque<int, Allocator>;

#if FUNNEL_CONCEPTS

static_assert(funnel::is_active_list<DisjointSetActiveVoidVector>);
static_assert(funnel::is_active_list<DisjointSetActiveVoidDeque>);
static_assert(!funnel::is_active_list_with_values<DisjointSetActiveVoidVector>);
static_assert(!funnel::is_active_list_with_values<DisjointSetActiveVoidDeque>);
static_assert(funnel::is_active_list_with_values<DisjointSetActiveIntVector>);
static_assert(funnel::is_active_list_with_values<DisjointSetActiveIntDeque>);

#endif

}  // unnamed namespace

TYPE_TO_STRING(DisjointSetActiveVoidVector);
TYPE_TO_STRING(DisjointSetActiveVoidDeque);
TYPE_TO_STRING(DisjointSetActiveIntVector);
TYPE_TO_STRING(DisjointSetActiveIntDeque);

TEST_SUITE_BEGIN("ActiveList");

TEST_CASE_TEMPLATE("active_list -- ", ActiveList, DisjointSetActiveVoidVector,
                   DisjointSetActiveVoidDeque, DisjointSetActiveIntVector,
                   DisjointSetActiveIntDeque) {
  Test<ActiveList> test;
  using Index = typename ActiveList::Index;

  SUBCASE("fillBack") { test.fillBack(test_list_size); }

  if constexpr (ActiveList::is_two_sided) {
    SUBCASE("fillFront") { test.fillFront(test_list_size); }
  }

  SUBCASE("fillRandom") { test.fillRandom(test_list_size); }

  SUBCASE("deactivateFront") {
    test.template fillRandom<false>(test_list_size);
    test.deactivateFront(test_list_size);
  }

  SUBCASE("deactivateBack") {
    test.template fillRandom<false>(test_list_size);
    test.deactivateBack(test_list_size);
  }

  SUBCASE("deactivateRange") {
    test.template fillRandom<false>(test_list_size);
    test.deactivateRange(test_list_size / 4, test_list_size * 3 / 4);
  }

  SUBCASE("deactivateRandom") {
    for (size_t i{0}; i < num_random_experiments; ++i) {
      test.template fillRandom<false>(test_list_size);
      test.deactivateRandom(test_list_size);
      test.clear();
    }
  }

  SUBCASE("fillAfterDeactivateAll") {
    test.template fillRandom<false>(test_list_size);
    REQUIRE_EQ(test.num_back_pushes + test.num_front_pushes, test_list_size);
    REQUIRE_EQ(test.num_deactivated, 0);
    test.deactivateAll();
    REQUIRE_EQ(test.num_back_pushes + test.num_front_pushes, test_list_size);
    REQUIRE_EQ(test.num_deactivated, test_list_size);
    test.fillRandom(test_list_size);
    REQUIRE_EQ(test.num_back_pushes + test.num_front_pushes,
               test_list_size * 2);
    REQUIRE_EQ(test.num_deactivated, test_list_size);
    test.deactivateAll();
    REQUIRE_EQ(test.num_back_pushes + test.num_front_pushes,
               test_list_size * 2);
    REQUIRE_EQ(test.num_deactivated, test_list_size * 2);
  }

  SUBCASE("randomizedOccupationTest") {
    for (size_t i{0}; i < num_random_experiments; ++i) {
      test.randomizedOccupationTest(test_list_size * 2);
      REQUIRE(test.num_deactivated == test_list_size);
      test.clear();
    }
  }

  SUBCASE("optimize_empty") {
    SUBCASE("with_tracking") { test.optimize(); }
    SUBCASE("without_tracking") { test.template optimize<false>(); }
  }

  SUBCASE("optimize_fill") {
    test.template fillRandom<false>(test_list_size);
    SUBCASE("with_tracking") {
      test.optimize();
      test.optimize();
    }
    SUBCASE("without_tracking") {
      test.template optimize<false>();
      test.template optimize<false>();
    }
  }

  SUBCASE("optimize_after_deactivateFront") {
    test.template fillRandom<false>(test_list_size);
    test.template deactivateFront<false>(test_list_size / 2);
    SUBCASE("with_tracking") {
      test.optimize();
      test.optimize();
    }
    SUBCASE("without_tracking") {
      test.template optimize<false>();
      test.template optimize<false>();
    }
  }

  SUBCASE("optimize_after_deactivateBack") {
    test.template fillRandom<false>(test_list_size);
    test.template deactivateBack<false>(test_list_size / 2);
    SUBCASE("with_tracking") {
      test.optimize();
      test.optimize();
    }
    SUBCASE("without_tracking") {
      test.template optimize<false>();
      test.template optimize<false>();
    }
  }

  SUBCASE("optimize_after_deactivateAll") {
    test.template fillRandom<false>(test_list_size);
    test.template deactivateAll<false>();
    SUBCASE("with_tracking") {
      test.optimize();
      test.optimize();
    }
    SUBCASE("without_tracking") {
      test.template optimize<false>();
      test.template optimize<false>();
    }
  }

  SUBCASE("optimize_after_deactivateRandom") {
    for (size_t i{0}; i < num_random_experiments; ++i) {
      test.template fillRandom<false>(test_list_size);
      test.template deactivateRandom<false>(test_list_size / 2);
      test.optimize();
      test.optimize();
      test.clear();
    }
  }
}

TEST_SUITE_END();

