#include <doctest/doctest.h>

#include "probes.hpp"

#include <funnel/active_list/disjoint_set.hpp>
#include <funnel/key_view/identity.hpp>
#include <funnel/layer/basic.hpp>
#include <funnel/layer_array/basic.hpp>
#include <funnel/layer_array/indexed.hpp>

#include <iostream>
#include <list>
#include <memory>
#include <random>
#include <vector>

using namespace std;

namespace /* unnamed */ {

// Test configurations

using Rng = mt19937_64;
static constexpr Rng::result_type rng_seed{12345678};
static constexpr size_t num_random_experiments{5};

using Value = int;
using KeyView = funnel::key_view::IdentityKeyView<Value>;
using Allocator = allocator<void*>;
using Layer =
    funnel::layer::BasicLayer<Value, Allocator,
                              funnel::active_list::DisjointSetActiveDeque>;
using LayerAllocator = allocator<Layer>;

template<class LayerArray1, class LayerArray2>
struct Test {
  using This = Test<LayerArray1, LayerArray2>;
  using LayerArray = LayerArray1;
  using OtherLayerArray = LayerArray2;
  using Probe = funnel::layer_array::Probe<LayerArray1>;
  LayerArray1 layer_array_1;
  LayerArray2 layer_array_2;
  KeyView key_view;
  Rng rng{rng_seed};

  using size_type = typename LayerArray1::size_type;
  using Id = typename LayerArray1::Id;
  using Index = typename LayerArray1::Index;

  constexpr Test() = default;
  constexpr Test(This const&) = default;
  constexpr Test(This&&) = default;
  constexpr This& operator=(This const&) = default;
  constexpr This& operator=(This&&) = default;

  constexpr void checkEqual(This& other) {
    Probe::checkEqual(layer_array_1, other.layer_array_1, key_view);
    Probe::checkEqual(layer_array_2, other.layer_array_2, key_view);
  }

  constexpr void checkCopies() {
    {  // Check copy and move constructors.
      This copy(*this);
      checkEqual(copy);
      This copy_2(std::move(copy));
      checkEqual(copy_2);
      copy.swap(copy_2);
      checkEqual(copy);
    }
    {  // Check copy and move assignments.
      This copy;
      copy = *this;
      checkEqual(copy);
      This copy_2;
      copy_2 = std::move(copy);
      copy.swap(copy_2);
      checkEqual(copy);
    }
  }

  template<bool check = true>
  constexpr void checkEqual() {
    if constexpr (check) {
      Probe::checkEqual(layer_array_1, layer_array_2, key_view);
    }
  }

  template<bool check = true>
  constexpr void clear() {
    layer_array_1.clear();
    layer_array_2.clear();
    checkEqual<check>();
  }

  template<bool check = true>
  constexpr void swap(Test& other) {
    layer_array_1.swap(other.layer_array_1);
    layer_array_2.swap(other.layer_array_2);
    checkEqual<check>();
    other.checkEqual<check>();
  }

  template<bool check = true>
  constexpr size_type numLayers() {
    if constexpr (check) {
      CHECK_EQ(layer_array_1.numLayers(), layer_array_2.numLayers());
    }
    return layer_array_1.numLayers();
  }

  template<bool check = true>
  constexpr Layer& newLayer() {
    auto& layer_1{layer_array_1.newLayer()};
    auto& layer_2{layer_array_2.newLayer()};
    checkEqual<check>();
    return layer_1;
  }

  template<bool check = true>
  constexpr void deleteLayer(Id id) {
    layer_array_1.deleteLayer(id);
    layer_array_2.deleteLayer(id);
    checkEqual<check>();
  }

  template<class ValueLocation1, class ValueLocation2>
  constexpr void ownValueLocations(ValueLocation1& value_location_1,
                                   ValueLocation2& value_location_2) {
    layer_array_1.ownValueLocation(value_location_1);
    layer_array_2.ownValueLocation(value_location_2);
  }

  template<bool check = true, class... Args>
  constexpr Index emplaceBack(Id id, Args const&... args) {
    Index index_1{layer_array_1.layer(id).emplaceBack(args...)};
    Index index_2{layer_array_2.layer(id).emplaceBack(args...)};
    if constexpr (check) {
      CHECK_EQ(index_1, index_2);
    }
    return index_1;
  }

  template<bool check = true, class... Args>
  constexpr Index emplaceFront(Id id, Args const&... args) {
    Index index_1{layer_array_1.layer(id).emplaceFront(args...)};
    Index index_2{layer_array_2.layer(id).emplaceFront(args...)};
    if constexpr (check) {
      CHECK_EQ(index_1, index_2);
    }
    return index_1;
  }

  template<bool check = true>
  constexpr Index erase(Id id, Index index) {
    Index index_1{layer_array_1.layer(id).erase(index)};
    Index index_2{layer_array_2.layer(id).erase(index)};
    if constexpr (check) {
      CHECK_EQ(index_1, index_2);
    }
    return index_1;
  }

  template<bool check = true>
  constexpr Index size(Id id) {
    bool size_1{layer_array_1.layer(id).size()};
    bool size_2{layer_array_2.layer(id).size()};
    if constexpr (check) {
      CHECK_EQ(size_1, size_2);
    }
    return size_1;
  }

  template<bool check = true>
  constexpr Index capacity(Id id) {
    Index capacity_1{layer_array_1.layer(id).capacity()};
    Index capacity_2{layer_array_2.layer(id).capacity()};
    if constexpr (check) {
      CHECK_EQ(capacity_1, capacity_2);
    }
    return capacity_1;
  }

  template<bool check = true>
  constexpr bool isValid(Id id, Index index) {
    bool valid_1{layer_array_1.layer(id).isValid(index)};
    bool valid_2{layer_array_2.layer(id).isValid(index)};
    if constexpr (check) {
      CHECK_EQ(valid_1, valid_2);
    }
    return valid_1;
  }

  template<bool check = true>
  void fillRandomLayer(Id id, size_t layer_size, Value min_value,
                       Value min_step, Value max_step) {
    uniform_int_distribution<Value> gen_step(min_step, max_step);
    Value value{min_value};
    for (size_t i{0}; i < layer_size; ++i) {
      value += gen_step(rng);
      emplaceBack<check>(id, value);
    }
  }

  template<bool check = true>
  void eraseRandomIndices(Id id, size_t max_num_removals) {
    uniform_int_distribution<Index> gen_index(0, capacity<check>(id) - 1);
    for (size_t i{0}; i < max_num_removals; ++i) {
      Index index{gen_index(rng)};
      if (isValid<check>(id, index)) {
        erase<check>(id, index);
      }
    }
  }

  template<bool check = true>
  void randomizeNewLayers(size_t num_layers, size_t layer_size,
                          size_t max_num_removals, Value min_value,
                          Value min_step, Value max_step) {
    for (size_t i{0}; i < num_layers; ++i) {
      newLayer<check>();
      Id id{numLayers() - 1};
      fillRandomLayer(id, layer_size, min_value, min_step, max_step);
      eraseRandomIndices(id, max_num_removals);
    }
    checkEqual<check>();
  }

  template<bool check = true>
  void randomizeDeleteLayers(size_t max_num_removals) {
    for (size_t i{0}; i < max_num_removals; ++i) {
      if (numLayers() == 0) {
        return;
      }
      uniform_int_distribution<Id> gen_id(0, numLayers() - 1);
      deleteLayer<check>(gen_id(rng));
    }
  }

  template<bool check = true>
  void randomizeNewAndDeleteLayers(size_t num_operations, size_t max_num_layers,
                                   size_t min_layer_size, size_t max_layer_size,
                                   Value min_step, Value max_step) {
    uniform_int_distribution<Id> gen_target_num_layers(0, max_num_layers);
    uniform_int_distribution<Index> gen_layer_size(min_layer_size,
                                                   max_layer_size);
    uniform_int_distribution<size_t> gen_op(0, 2);
    for (size_t i{0}; i < num_operations; ++i) {
      if (numLayers<false>() == 0) {
        Index layer_size{gen_layer_size(rng)};
        Index max_num_removals{
            uniform_int_distribution<Index>(0, layer_size)(rng)};
        randomizeNewLayers<false>(1, layer_size, max_num_removals, 0, min_step,
                                  max_step);
      }
      Id target_num_layers{gen_target_num_layers(rng)};
      if (numLayers<false>() < target_num_layers) {
        Index layer_size{gen_layer_size(rng)};
        Index max_num_removals{
            uniform_int_distribution<Index>(0, layer_size)(rng)};
        randomizeNewLayers<check>(1, layer_size, max_num_removals, 0, min_step,
                                  max_step);
      } else if (numLayers<false>() > target_num_layers) {
        randomizeDeleteLayers<check>(1);
      }
    }
  }
};

using VectorBasicLayerArray =
    funnel::layer_array::BasicLayerArray<Layer, LayerAllocator, vector>;
using ListIndexedLayerArray =
    funnel::layer_array::IndexedLayerArray<Layer, LayerAllocator, list>;

using TestVectorBasicLayerArray =
    Test<VectorBasicLayerArray, ListIndexedLayerArray>;
using TestListIndexedLayerArray =
    Test<ListIndexedLayerArray, VectorBasicLayerArray>;

}  // unnamed namespace

TYPE_TO_STRING(TestVectorBasicLayerArray);
TYPE_TO_STRING(TestListIndexedLayerArray);

TEST_SUITE_BEGIN("layer_array");

TEST_CASE_TEMPLATE("layer_array -- ", TestT, TestVectorBasicLayerArray,
                   TestListIndexedLayerArray) {
  using T = TestT;
  T test;

  SUBCASE("check empty layers") { test.checkCopies(); }

  SUBCASE("check newLayer") {
    test.randomizeNewLayers(100, 10, 10, -5, 0, 2);
    test.checkCopies();
  }

  SUBCASE("check deleteLayer") {
    test.template randomizeNewLayers<false>(100, 10, 10, -5, 0, 2);
    test.randomizeDeleteLayers(100);
  }

  SUBCASE("randomize newLayer and deleteLayer") {
    for (size_t i{0}; i < 5; ++i) {
      test.randomizeNewAndDeleteLayers(1000, 20, 0, 20, -2, 2);
      test.clear();
    }
  }
}

TEST_SUITE_END();
