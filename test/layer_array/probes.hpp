#pragma once

#include <doctest/doctest.h>

#include <funnel/layer_array/basic.hpp>
#include <funnel/layer_array/indexed.hpp>

namespace funnel::layer_array {

class BasicLayerArrayProbe {
public:
};

class IndexedLayerArrayProbe {};

template<class LayerArray>
struct Probe;

template<>
struct Probe<void> {
  template<class LayerArray1, class LayerArray2, class KeyView>
  static constexpr void checkEqual(LayerArray1& layer_array_1,
                                   LayerArray2& layer_array_2,
                                   KeyView const& key_view) {
    REQUIRE_EQ(layer_array_1.numLayers(), layer_array_2.numLayers());
    for (typename LayerArray1::size_type id{0}; id < layer_array_1.numLayers();
         ++id) {
      CHECK_EQ(
          layer_array_1.layer(id).compare(layer_array_2.layer(id), key_view),
          0);
    }
  }
};

template<class LA>
struct Probe : public Probe<void> {
  using Super = Probe<void>;
  using Super::checkEqual;
};

}  // namespace funnel::layer_array
