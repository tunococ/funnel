FROM tunococ/ubuntu_cmake:focal_3.16.3
ARG USE_CPP_VERSION=20
COPY . .
WORKDIR /build
RUN cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_STANDARD="${USE_CPP_VERSION}" ..
RUN cmake --build .
ENTRYPOINT ["ctest", "--output-on-failure"]
