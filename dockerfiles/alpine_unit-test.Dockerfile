FROM tunococ/alpine_cmake:3.16.0_3.23.1-r0
ARG USE_CPP_VERSION=20
COPY . .
WORKDIR /build
RUN cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_STANDARD="${USE_CPP_VERSION}" ..
RUN cmake --build .
ENTRYPOINT ["ctest", "--output-on-failure"]
