# Funnel

A **funnel** is a data structure for a collection of objects that are ordered.
It functions like most search tree and skip list data structures,
  but may be more efficient in cases where
  the collection size is relatively small
  due to cache locality and fewer pointer dereferences.

However, a funnel has asymptotic time bounds for most common
  operations&mdash;$O(\log^2 n)$ instead of $O(\log n)$, where
  $n$ is the size of the collection&mdash;so
  it will perform worse than other well-known $O(\log n)$ data structures when
  the collection size is large.

## Unit Test

### Building and running unit test

A standard way to initialize the build directory with CMake is as follows:
```
repo_root> cmake -S . -B build -DCMAKE_BUILD_TYPE=Debug
repo_root> cd build
```
Once inside the `build` directory, the unit test code can be built with
```
repo_root/build> cmake --build ..
```
This will build the unit test binary, which can be executed directly or via CTest as follows:
```
repo_root/build> ctest
```

### Testing in a docker container

From the root of this repository, execute
```
repo_root> script/unit-test-docker.sh
```
This will run the unit test in a default docker image.

This script supports some custom arguments. To see more details, run
```
repo_root> script/unit-test-docker.sh --help
```

### Code coverage

To generate coverage information, toggle the option `FUNNEL_TEST_COVERAGE` on in CMake.
This can be done by modifying `CMakeCache.txt` in the `build` directory directly, by
  executing
```
repo_root/build> cmake -DFUNNEL_TEST_COVERAGE=1 .
```
  while inside the `build` directory, or by adding `-DFUNNEL_TEST_COVERAGE=1` during
  the initialization of the build directory:
```
repo_root> cmake -S . -B build -DCMAKE_BUILD_TYPE=Debug -DFUNNEL_TEST_COVERAGE=1
```

Afterwards, three build targets may become available.
- `unit_test_lcov` will be available if `lcov` is available on the system.
  Building this target will generate coverage information from `lcov`:
  ```
  repo_root/build> cmake --build . --target=unit_test_lcov
  ```
- `unit_test_gcovr_xml` will be availabe if `gcovr` is available on the system.
  Building this target will generate coverage information in XML format from `gcovr`:
  ```
  repo_root/build> cmake --build . --target=unit_test_gcovr_xml
  ```
- `unit_test_gcovr_html` will be availabe if `gcovr` is available on the system.
  Building this target will generate coverage information in HTML format from `gcovr`:
  ```
  repo_root/build> cmake --build . --target=unit_test_gcovr_html
  ```
