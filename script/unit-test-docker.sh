#!/bin/bash

set -e

BUILD_ARGS=""

IMAGE_PREFIX="alpine"
IMAGE_SUFFIX=""
while true; do
  if [[ "$1" == '--help' ]] ; then
    echo "Usage: unit-test-docker.sh [-17 | -20] [-I image_prefix] [BUILD_ARGS...]"
    echo "Arguments:"
    echo "  -17              Use C++17"
    echo "  -20              Use C++20"
    echo "  -I image_prefix  Use dockerfiles/\$\{image_prefix\}_debug.Dockerfile as the docker image"
    echo "  BUILD_ARGS...    All the remaining arguments are passed to docker build"
    echo "Examples:"
    echo "> script/unit-test-docker.sh -17 -I ubuntu"
    echo "    - Builds and runs the unit test in the container specified by the image file"
    echo "      dockerfiles/ubuntu_debug.Dockerfile using C++17 as the standard."
    exit 0
  elif [[ "$1" == '-17' ]] ; then
    BUILD_ARGS="--build-arg USE_CPP_VERSION=17"
    shift
  elif [[ "$1" == '-20' ]] ; then
    BUILD_ARGS="--build-arg USE_CPP_VERSION=20"
    shift
  elif [[ "$1" == '-I' ]] ; then
    shift
    IMAGE_PREFIX="$1"
    shift
  else
    break
  fi
done

DOCKERFILE="dockerfiles/${IMAGE_PREFIX}_unit-test${IMAGE_SUFFIX}.Dockerfile"
IMAGE_TAG="funnel-unit-test-local-image"

echo ">>> docker build -t ${IMAGE_TAG} ${BUILD_ARGS} -f ${DOCKERFILE} ."
docker build -t "${IMAGE_TAG}" ${BUILD_ARGS} -f ${DOCKERFILE} .

echo ">>> docker run --rm ${IMAGE_TAG} $@"
if docker run --rm "${IMAGE_TAG}" $@ ; then
  docker image rm ${IMAGE_TAG} &> /dev/null
else
  docker image rm ${IMAGE_TAG} &> /dev/null
  exit 1
fi
