#pragma once

#include "../key_view/concepts.hpp"
#include "../tracker/index.hpp"
#include "concepts.hpp"

#include <cassert>
#include <cstddef>
#include <memory>

namespace funnel::layer::merger {

using funnel::tracker::rawPointer;
using funnel::tracker::TrackedIndices;
using funnel::tracker::trackIndices;

template<class LayerT>
FUNNEL_REQUIRES((is_layer<LayerT>))
struct Merger {
  using Layer = LayerT;
  using Index = typename Layer::Index;
  using TrackNothing = TrackedIndices<Index, 0>;

  template<std::size_t M, std::size_t N, class KeyView>
  static constexpr void startMerge(
      Layer& output, Layer& first, Layer& second,
      TrackedIndices<Index, M>* tracked_first_indices,
      TrackedIndices<Index, N>* tracked_second_indices,
      KeyView const& key_view) {
    output.clear();
    merge(output, first, first.begin(), second, second.begin(),
          tracked_first_indices, tracked_second_indices, key_view);
  }

  template<std::size_t M, class KeyView>
  static constexpr void startMerge(
      Layer& output, Layer& first, Layer& second,
      TrackedIndices<Index, M>* tracked_first_indices, std::nullptr_t,
      KeyView const& key_view) {
    TrackNothing track_nothing{};
    startMerge(output, first, second, tracked_first_indices, &track_nothing,
               key_view);
  }

  template<std::size_t N, class KeyView>
  static constexpr void startMerge(
      Layer& output, Layer& first, Layer& second, std::nullptr_t,
      TrackedIndices<Index, N>* tracked_second_indices,
      KeyView const& key_view) {
    TrackNothing track_nothing{};
    startMerge(output, first, second, &track_nothing, tracked_second_indices,
               key_view);
  }

  template<class KeyView>
  static constexpr void startMerge(Layer& output, Layer& first, Layer& second,
                                   std::nullptr_t, std::nullptr_t,
                                   KeyView const& key_view) {
    using TrackNothing = TrackedIndices<Index, 0>;
    TrackNothing track_nothing{};
    startMerge(output, first, second, &track_nothing, &track_nothing, key_view);
  }

  /** @brief
   *  Copies a value from `layer[index]` into `output`, attempts to update
   *  `tracked_indices`, and returns `true` if a tracked index has been checked.
   */
  template<std::size_t N>
  static constexpr bool copyValue(Layer& output, Layer& layer, Index& index,
                                  TrackedIndices<Index, N>* tracked_indices) {
    if constexpr (N > 0) {
      Index old_index{index};
      index = layer.next(old_index);
      Index output_index{output.emplaceBack(std::move(layer[old_index]))};
      layer.erase(old_index);
      if (old_index < tracked_indices->head) {
        return false;
      }
      tracked_indices->head = output_index;
      return true;
    } else {
      Index old_index{index};
      index = layer.next(old_index);
      output.emplaceBack(std::move(layer[old_index]));
      layer.erase(old_index);
      return false;
    }
  }

  template<std::size_t N>
  static constexpr void copyLayer(Layer& output, Layer& layer, Index index,
                                  TrackedIndices<Index, N>* tracked_indices) {
    while (index != layer.end()) {
      if (copyValue(output, layer, index, tracked_indices)) {
        if constexpr (N > 0) {
          return copyLayer(output, layer, index,
                           tracked_indices->tailPointer());
        }
      }
    }
    trackEnds(output, tracked_indices);
  }

  template<std::size_t N>
  static constexpr void trackEnds(Layer& output,
                                  TrackedIndices<Index, N>* tracked_indices) {
    if constexpr (N > 0) {
      tracked_indices->head = output.end();
      trackEnds(output, tracked_indices->tailPointer());
    }
  }

  template<std::size_t M, std::size_t N, class KeyView>
  static constexpr void merge(Layer& output, Layer& first_layer,
                              Index first_index, Layer& second_layer,
                              Index second_index,
                              TrackedIndices<Index, M>* tracked_first_indices,
                              TrackedIndices<Index, N>* tracked_second_indices,
                              KeyView const& key_view) {
    while (true) {
      if (first_index == first_layer.end()) {
        copyLayer(output, second_layer, second_index, tracked_second_indices);
        trackEnds(output, tracked_first_indices);
        return;
      }
      if (second_index == second_layer.end()) {
        copyLayer(output, first_layer, first_index, tracked_first_indices);
        trackEnds(output, tracked_second_indices);
        return;
      }
      if (key_view.compare(second_layer[second_index],
                           first_layer[first_index])) {
        if (copyValue(output, second_layer, second_index,
                      tracked_second_indices)) {
          if constexpr (N > 0) {
            return merge(output, first_layer, first_index, second_layer,
                         second_index, tracked_first_indices,
                         tracked_second_indices->tailPointer(), key_view);
          }
        }
        continue;
      }
      if (copyValue(output, first_layer, first_index, tracked_first_indices)) {
        if constexpr (M > 0) {
          return merge(output, first_layer, first_index, second_layer,
                       second_index, tracked_first_indices->tailPointer(),
                       tracked_second_indices, key_view);
        }
      }
    }
  }
};

/** @brief
 *  Merges two layers and track indices in those layers.
 *
 *  `trackIndices()` can be used to generate @p tracked_a_indices and
 *  @p tracked_b_indices .
 *
 *  @param a First layer to merge.
 *  @param b Second layer to merge.
 *  @param[in,out] tracked_a_indices Tracked indices in @p a .
 *    The type of @p tracked_a_indices should be a pointer to
 *    `TrackedIndices<Index, M>` for some `M` and a compatible `Index`.
 *  @param[in,out] tracked_b_indices Tracked indices in @p b .
 *    The type of @p tracked_b_indices should be a pointer to
 *    `TrackedIndices<Index, N>` for some `N` and a compatible `Index`.
 *  @param key_view `KeyView` object used in comparison.
 *  @param allocator Allocator for the resulting layer.
 *  @returns Merged layer.
 */
template<class Layer, class TrackedIndices1, class TrackedIndices2,
         class KeyView, class Allocator>
FUNNEL_REQUIRES((is_layer<Layer>)&&(is_key_view<KeyView>))
constexpr Layer merge(Layer& a, Layer& b, TrackedIndices1&& tracked_a_indices,
                      TrackedIndices2&& tracked_b_indices,
                      KeyView const& key_view, Allocator const& allocator) {
  Layer output{allocator};
  Merger<Layer>::startMerge(output, a, b, rawPointer(tracked_a_indices),
                            rawPointer(tracked_b_indices), key_view);
  return output;
}

/** @brief
 *  Merges two layers and track indices in those layers.
 *
 *  `trackIndices()` can be used to generate @p tracked_a_indices and
 *  @p tracked_b_indices .
 *
 *  The allocator of @p a will be used for the output layer.
 *
 *  @param a First layer to merge.
 *  @param b Second layer to merge.
 *  @param[in,out] tracked_a_indices Tracked indices in @p a .
 *    The type of @p tracked_a_indices should be a pointer to
 *    `TrackedIndices<Index, M>` for some `M` and a compatible `Index`.
 *  @param[in,out] tracked_b_indices Tracked indices in @p b .
 *    The type of @p tracked_b_indices should be a pointer to
 *    `TrackedIndices<Index, N>` for some `N` and a compatible `Index`.
 *  @param key_view `KeyView` object used in comparison.
 *  @returns Merged layer.
 */
template<class Layer, class TrackedIndices1, class TrackedIndices2,
         class KeyView>
FUNNEL_REQUIRES((is_layer<Layer>)&&(is_key_view<KeyView>))
constexpr Layer
    merge(Layer& a, Layer& b, TrackedIndices1&& tracked_a_indices,
          TrackedIndices2&& tracked_b_indices, KeyView const& key_view) {
  return funnel::layer::merger::merge(
      a, b, std::forward<TrackedIndices1>(tracked_a_indices),
      std::forward<TrackedIndices2>(tracked_b_indices), key_view,
      a.getAllocator());
}

/** @brief
 *  Merges two layers.
 *
 *  @param a First layer to merge.
 *  @param b Second layer to merge.
 *  @param key_view `KeyView` object used in comparison.
 *  @param allocator Allocator for the resulting layer.
 *  @returns Merged layer.
 */
template<class Layer, class KeyView, class Allocator>
FUNNEL_REQUIRES((is_layer<Layer>)&&(is_key_view<KeyView>))
constexpr Layer merge(Layer& a, Layer& b, KeyView const& key_view,
                      Allocator const& allocator) {
  return funnel::layer::merger::merge(a, b, nullptr, nullptr, key_view,
                                      allocator);
}

/** @brief
 *  Merges two layers.
 *
 *  The allocator of @p a will be used for the output layer.
 *
 *  @param a First layer to merge.
 *  @param b Second layer to merge.
 *  @param key_view `KeyView` object used in comparison.
 *  @returns Merged layer.
 */
template<class Layer, class KeyView>
FUNNEL_REQUIRES((is_layer<Layer>)&&(is_key_view<KeyView>))
constexpr Layer merge(Layer& a, Layer& b, KeyView const& key_view) {
  return funnel::layer::merger::merge(a, b, key_view, a.getAllocator());
}

}  // namespace funnel::layer::merger

namespace funnel::layer {

using merger::merge;

}  // namespace funnel::layer

