#pragma once

#include "../concepts.hpp"
#include "../key_view/concepts.hpp"

#if FUNNEL_CONCEPTS

#include <tuple>
#include <type_traits>

namespace funnel {

template<class Layer>
concept is_layer = requires(Layer layer, typename Layer::Index index) {
  typename Layer::Allocator;
  typename Layer::Value;
  typename Layer::ValuePointer;
  { layer.getAllocator() } -> std::convertible_to<typename Layer::Allocator>;
  { layer.size() } -> std::convertible_to<typename Layer::size_type>;
  { layer.capacity() } -> std::convertible_to<typename Layer::size_type>;
  {layer.clear()};
  { layer.empty() } -> std::convertible_to<bool>;
  { layer.begin() } -> std::convertible_to<typename Layer::Index>;
  { layer.end() } -> std::convertible_to<typename Layer::Index>;
  { layer.frontIndex() } -> std::convertible_to<typename Layer::Index>;
  { layer.backIndex() } -> std::convertible_to<typename Layer::Index>;
  { layer.next(index) } -> std::convertible_to<typename Layer::Index>;
  { layer.prev(index) } -> std::convertible_to<typename Layer::Index>;
  { layer.isValid(index) } -> std::convertible_to<bool>;
  {
    layer[index]
    }
    -> std::convertible_to<std::add_lvalue_reference_t<typename Layer::Value>>;
  {
    layer.front()
    }
    -> std::convertible_to<std::add_lvalue_reference_t<typename Layer::Value>>;
  {
    layer.back()
    }
    -> std::convertible_to<std::add_lvalue_reference_t<typename Layer::Value>>;
};

template<class Layer, class KeyView>
concept is_searchable_layer = requires(Layer layer,
                                       typename KeyView::Key const& key,
                                       KeyView const& key_view) {
  requires is_layer<Layer>;
  requires is_key_view<KeyView>;
  {
    layer.lowerBound(key, key_view)
    } -> std::convertible_to<typename Layer::Index>;
  {
    layer.upperBound(key, key_view)
    } -> std::convertible_to<typename Layer::Index>;
  {
    layer.equalRange(key, key_view)
    } -> std::convertible_to<
        std::tuple<typename Layer::Index, typename Layer::Index>>;
  {
    layer.count(key, key_view)
    } -> std::convertible_to<typename Layer::size_type>;
  { layer.find(key, key_view) } -> std::convertible_to<typename Layer::Index>;
  {
    layer.find(key, key_view, []() { return true; })
    } -> std::convertible_to<typename Layer::Index>;
  { layer.contains(key, key_view) } -> std::convertible_to<bool>;
};

}  // namespace funnel

#endif
