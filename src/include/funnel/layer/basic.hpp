#pragma once

#include "../active_list/concepts.hpp"
#include "../key_view/concepts.hpp"
#include "concepts.hpp"
#include "merger.hpp"

#include <cassert>
#include <memory>

namespace funnel::layer {

template<class ValueT, class AllocatorT,
         template<class, class> class ActiveListF>
FUNNEL_REQUIRES((funnel::is_active_list<ActiveListF<ValueT, AllocatorT>>))
class BasicLayer : protected ActiveListF<ValueT, AllocatorT> {
public:
  using Value = ValueT;
  using Allocator = AllocatorT;
  using ActiveList = ActiveListF<Value, Allocator>;
  using Index = typename ActiveList::Index;
  using size_type = typename ActiveList::size_type;
  static constexpr bool is_two_sided{ActiveList::is_two_sided};

  using ValuePointer = typename std::allocator_traits<Allocator>::pointer;

  using Super = ActiveList;
  using This = BasicLayer<Value, Allocator, ActiveListF>;

  constexpr BasicLayer() = default;
  constexpr BasicLayer(Allocator const& allocator) noexcept(noexcept(Super{
      allocator}))
      : Super{allocator} {}
  constexpr BasicLayer(This const&) = default;
  constexpr BasicLayer(This&&) = default;
  constexpr This& operator=(This const&) = default;
  constexpr This& operator=(This&&) = default;

  constexpr Allocator getAllocator() const noexcept {
    return Super::getAllocator();
  }

  constexpr void clear() noexcept(noexcept(std::declval<Super>().clear())) {
    Super::clear();
  }

  constexpr bool empty() noexcept { return Super::numActive() == 0; }

  constexpr size_type size() noexcept { return Super::numActive(); }

  constexpr size_type capacity() noexcept { return Super::capacity(); }

  constexpr Index begin() noexcept {
    return empty() ? Super::beginActiveIndex() : frontIndex();
  }

  constexpr Index end() noexcept { return Super::endActiveIndex(); }

  constexpr Index next(Index index) noexcept {
    return Super::nextActiveIndex(index);
  }

  constexpr Index prev(Index index) noexcept {
    return Super::prevActiveIndex(index);
  }

  constexpr bool isValid(Index index) noexcept {
    return !empty() && (index >= frontIndex()) && (index <= backIndex()) &&
           Super::isActive(index);
  }

  constexpr Value& operator[](Index index) noexcept {
    assert(isValid(index) && "invalid index");
    return Super::valueAt(index);
  }

  constexpr Index frontIndex() noexcept { return Super::frontActiveIndex(); }

  constexpr Index backIndex() noexcept { return Super::backActiveIndex(); }

  template<class... Args>
  constexpr Index emplaceFront(Args&&... args) noexcept(
      noexcept(std::declval<Super>().emplaceFront(args...)))
      FUNNEL_REQUIRES(is_two_sided) {
    if constexpr (is_two_sided) {
      return Super::emplaceFront(std::forward<Args>(args)...);
    } else {
      assert(false && "one-sided layer cannot emplaceFront()");
      throw;
    }
  }

  template<class... Args>
  constexpr Index emplaceBack(Args&&... args) noexcept(
      noexcept(std::declval<Super>().emplaceBack(args...))) {
    return Super::emplaceBack(std::forward<Args>(args)...);
  }

  constexpr Value& front() noexcept {
    assert(!empty() && "front() cannot be called when the layer is empty");
    return operator[](frontIndex());
  }

  constexpr Value& back() noexcept {
    assert(!empty() && "back() cannot be called when the layer is empty");
    return operator[](backIndex());
  }

  constexpr void popFront() noexcept {
    assert(!empty() && "popFront() cannot be called when the layer is empty");
    Super::deactivate(frontIndex());
  }

  constexpr void popBack() noexcept {
    assert(!empty() && "popBack() cannot be called when the layer is empty");
    Super::deactivate(backIndex());
  }

  constexpr Index erase(Index index) noexcept {
    assert(isValid(index) && "invalid index");
    Super::deactivate(index);
    return Super::rightActiveBeginIndex(index);
  }

  constexpr Index erase(Index first_index, Index last_index) noexcept {
    assert((first_index >= begin()) && "first_index cannot be below begin()");
    assert((last_index <= end()) && "last_index cannot be above end()");
    while (first_index < last_index) {
      first_index = erase(first_index);
    }
    return first_index;
  }

  // Optimize and merge functions

  template<class TrackedIndicesT>
  constexpr void optimize(TrackedIndicesT&& tracked_indices = nullptr) noexcept(
      noexcept(Super::optimize(tracked_indices))) {
    Super::optimize(tracked_indices);
  }

  template<class TrackedIndices, class TrackedOtherIndices, class KeyView>
  constexpr This mergeWith(This& other, TrackedIndices&& tracked_indices,
                           TrackedOtherIndices&& tracked_other_indices,
                           KeyView const& key_view) {
    This output{merger::merge(
        *this, other, std::forward<TrackedIndices>(tracked_indices),
        std::forward<TrackedOtherIndices>(tracked_other_indices), key_view)};
    clear();
    other.clear();
    return output;
  }

  template<class KeyView>
  constexpr This mergeWith(This& other, KeyView const& key_view) {
    return mergeWith(other, merger::trackIndices(), merger::trackIndices(),
                     key_view);
  }

  // Search functions

  template<class Key, class KeyView>
  constexpr Index lowerBound(Key const& key, KeyView const& key_view) noexcept
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    return lowerBoundActiveIndex(key, Super::beginIndex(), Super::endIndex(),
                                 key_view);
  }

  template<class Key, class KeyView>
  constexpr Index upperBound(Key const& key, KeyView const& key_view) noexcept
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    return upperBoundActiveIndex(key, Super::beginIndex(), Super::endIndex(),
                                 key_view);
  }

  template<class Key, class KeyView>
  constexpr Index find(Key const& key, KeyView const& key_view) noexcept
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    return findActiveIndex(key, Super::beginIndex(), Super::endIndex(),
                           key_view);
  }

  template<class Key, class KeyView, class Continue>
  constexpr Index find(Key const& key, KeyView const& key_view,
                       Continue&& cont) noexcept
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    return findActiveIndex(key, Super::beginIndex(), Super::endIndex(),
                           key_view, cont);
  }

  template<class Key, class KeyView>
  constexpr std::pair<Index, Index> equalRange(Key const& key,
                                               KeyView const& key_view) noexcept
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    return equalRangeActiveIndices(key, Super::beginIndex(), Super::endIndex(),
                                   key_view);
  }

  template<class Key, class KeyView>
  constexpr size_type count(Key const& key, KeyView const& key_view) noexcept
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    std::pair<Index, Index> equal_range{equalRange(key, key_view)};
    return countActiveEntriesInRange(equal_range.first, equal_range.second);
  }

  template<class Key, class KeyView>
  constexpr bool contains(Key const& key, KeyView const& key_view) noexcept
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    return find(key, key_view) != end();
  }

  template<class Other, class KeyView>
  constexpr int compare(Other& other, KeyView const& key_view) noexcept
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    Index index{begin()};
    auto other_index{other.begin()};
    for (; index != end();
         index = next(index), other_index = other.next(other_index)) {
      if (other_index == other.end()) {
        return 1;
      }
      auto& value{operator[](index)};
      auto& other_value{other[other_index]};
      if (key_view.compare(value, other_value)) {
        return -1;
      }
      if (key_view.compare(other_value, value)) {
        return 1;
      }
    }
    if (other_index != other.end()) {
      return -1;
    }
    return 0;
  }

protected:
  template<class Key, class KeyView>
  constexpr Index lowerBoundIndex(Key const& key, Index left_index,
                                  Index right_index,
                                  KeyView const& key_view) noexcept
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    while (left_index < right_index) {
      Index mid_index{(left_index + right_index) / 2};
      Value const& mid_value{Super::valueAt(mid_index)};
      if (key_view.compare(mid_value, key)) {
        left_index = mid_index + 1;
      } else {
        right_index = mid_index;
      }
    }
    return right_index;
  }

  template<class Key, class KeyView>
  constexpr Index lowerBoundActiveIndex(Key const& key, Index left_index,
                                        Index right_index,
                                        KeyView const& key_view) noexcept
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    Index index{lowerBoundIndex(key, left_index, right_index, key_view)};
    if (index == right_index || Super::isActive(index)) {
      return index;
    }
    return Super::rightActiveBeginIndex(index);
  }

  template<class Key, class KeyView>
  constexpr Index upperBoundIndex(Key const& key, Index left_index,
                                  Index right_index,
                                  KeyView const& key_view) noexcept
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    while (left_index < right_index) {
      Index mid_index{(left_index + right_index) / 2};
      Value const& mid_value{Super::valueAt(mid_index)};
      if (key_view.compare(key, mid_value)) {
        right_index = mid_index;
      } else {
        left_index = mid_index + 1;
      }
    }
    return right_index;
  }

  template<class Key, class KeyView>
  constexpr Index upperBoundActiveIndex(Key const& key, Index left_index,
                                        Index right_index,
                                        KeyView const& key_view) noexcept
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    Index index{upperBoundIndex(key, left_index, right_index, key_view)};
    if (index == right_index || Super::isActive(index)) {
      return index;
    }
    return Super::rightActiveBeginIndex(index);
  }

  template<class Key, class KeyView, class Continue>
  constexpr Index findIndex(Key const& key, Index& left_index,
                            Index& right_index, KeyView const& key_view,
                            Continue&& cont) noexcept
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    Index end_index{right_index};
    while (cont() && left_index < right_index) {
      Index mid_index{(left_index + right_index) / 2};
      Value const& mid_value{Super::valueAt(mid_index)};
      if (key_view.compare(mid_value, key)) {
        left_index = mid_index + 1;
      } else if (key_view.compare(key, mid_value)) {
        right_index = mid_index;
      } else {
        return mid_index;
      }
    }
    return end_index;
  }

  template<class Key, class KeyView, class Continue>
  constexpr Index findActiveIndex(Key const& key, Index left_index,
                                  Index right_index, KeyView const& key_view,
                                  Continue&& cont) noexcept
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    Index end_index{right_index};
    Index found_index{findIndex(key, left_index, right_index, key_view, cont)};

    if (found_index == end_index || Super::isActive(found_index)) {
      return found_index;
    }

    Index right_mid_index{Super::rightActiveBeginIndex(found_index)};
    if (right_mid_index < right_index &&
        !key_view.compare(key, Super::valueAt(right_mid_index)) &&
        !key_view.compare(Super::valueAt(right_mid_index), key)) {
      return right_mid_index;
    }

    Index left_mid_index{Super::leftActiveEndIndex(found_index)};
    if (left_mid_index <= left_index) {
      return end_index;
    }
    --left_mid_index;
    if (!key_view.compare(key, Super::valueAt(left_mid_index)) &&
        !key_view.compare(Super::valueAt(left_mid_index), key)) {
      return left_mid_index;
    }
    return end_index;
  }

  template<class Key, class KeyView>
  constexpr Index findIndex(Key const& key, Index& left_index,
                            Index& right_index,
                            KeyView const& key_view) noexcept
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    return findIndex(key, left_index, right_index, key_view,
                     []() { return true; });
  }

  template<class Key, class KeyView>
  constexpr Index findActiveIndex(Key const& key, Index left_index,
                                  Index right_index,
                                  KeyView const& key_view) noexcept
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    return findActiveIndex(key, left_index, right_index, key_view,
                           []() { return true; });
  }

  template<class Key, class KeyView>
  constexpr std::pair<Index, Index> equalRangeActiveIndices(
      Key const& key, Index left_index, Index right_index,
      KeyView const& key_view) noexcept FUNNEL_REQUIRES(is_key_view<KeyView>) {
    Index lower_bound_index{
        lowerBoundActiveIndex(key, left_index, right_index, key_view)};
    if (lower_bound_index >= right_index) {
      return std::make_pair(right_index, right_index);
    }
    Index upper_bound_index{
        upperBoundActiveIndex(key, lower_bound_index, right_index, key_view)};
    return std::make_pair(lower_bound_index, upper_bound_index);
  }

  constexpr size_type countActiveEntriesInRange(Index from, Index to) noexcept {
    assert((from <= to) &&
           "the lower index of a range must not exceed the upper index");
    if (from == to || from >= Super::endIndex()) {
      return 0;
    }
    assert(isValid(from) &&
           "the lower index of a non-empty range must "
           "point to an active entry");
    size_type count{0};
    for (; from < to; from = next(from)) {
      ++count;
    }
    return count;
  }

  // For testing.
  friend class BasicLayerProbe;
};

}  // namespace funnel::layer
