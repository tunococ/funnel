#pragma once

#include <type_traits>

#if __cplusplus >= 202002L

#include <concepts>

#define FUNNEL_CONCEPTS 1

#define FUNNEL_REQUIRES(...) requires __VA_ARGS__

#define FUNNEL_DEFINE_HAS_MEMBER_FUNCTION(Function)       \
  template<class A, class... Args>                        \
  static constexpr bool _has_member_function_##Function = \
      requires(A a, Args... args) {                       \
    a(args...);                                           \
  };

#endif  // __cplusplus >= 202002L

// Fallback implementations

#ifndef FUNNEL_REQUIRES
#define FUNNEL_REQUIRES(...)
#endif

#ifndef FUNNEL_DEFINE_HAS_MEMBER_FUNCTION
#define FUNNEL_DEFINE_HAS_MEMBER_FUNCTION(Function)                    \
  struct _has_member_function_impl_##Function {                        \
    template<class...>                                                 \
    using always_true = std::true_type;                                \
                                                                       \
    template<class... Args>                                            \
    static constexpr std::false_type f(Args...);                       \
                                                                       \
    template<class A, class... Args>                                   \
    static constexpr always_true<                                      \
        decltype(std::declval<A>().Function(std::declval<Args>()...))> \
    f(int, A a, Args... args);                                         \
  };                                                                   \
                                                                       \
  template<class A, class... Args>                                     \
  static constexpr bool _has_member_function_##Function{               \
      decltype(_has_member_function_impl_##Function::template f(       \
          0, std::declval<A>(), std::declval<Args>()...))::value};
#endif
