#pragma once

/** @file
 *  This file contains common basic algorithms for constructing a ValueLocation.
 */

#include "../key_view/concepts.hpp"

#include <cassert>
#include <future>
#include <memory>
#include <vector>

namespace funnel::value_location {

template<class Location, class KeyView, class GenIndex>
constexpr void buildLocation(Location& location, KeyView const& key_view,
                             GenIndex&& gen_index)
    FUNNEL_REQUIRES(is_key_view<KeyView>) {
  typename Location::LayerArray& layers{*location.getLayerArray()};
  for (typename Location::Id id{0}; id < layers.numLayers(); ++id) {
    location.setLayerIndex(id, gen_index(layers.layer(id), id));
  }
  location.build(key_view);
}

template<class Location, class KeyView, class GenIndex,
         template<class, class> class TaskList = std::vector>
constexpr void buildLocation(Location& location, KeyView const& key_view,
                             GenIndex&& gen_index, std::launch policy)
    FUNNEL_REQUIRES(is_key_view<KeyView>) {
  typename Location::LayerArray& layers{*location.getLayerArray()};
  TaskList<std::future<void>, std::allocator<std::future<void>>> tasks(
      layers.numLayers());
  for (typename Location::Id id{0}; id < layers.numLayers(); ++id) {
    tasks[id] = std::async(
        policy, [&location, id, &gen_index, &layer = layers.layer(id)]() {
          location.setLayerIndex(id, gen_index(layer, id));
        });
  }
  for (typename Location::Id id{0}; id < layers.numLayers(); ++id) {
    tasks[id].wait();
  }
  location.build(key_view);
}

template<class Location, class KeyView, class... Launch>
constexpr void buildBeginLocation(Location& location, KeyView const& key_view,
                                  Launch... launch)
    FUNNEL_REQUIRES(is_key_view<KeyView>) {
  buildLocation(
      location, key_view,
      [&key_view](auto& layer, auto) { return layer.begin(); }, launch...);
}

template<class Location, class KeyView, class... Launch>
constexpr void buildEndLocation(Location& location, KeyView const& key_view,
                                Launch... launch)
    FUNNEL_REQUIRES(is_key_view<KeyView>) {
  buildLocation(
      location, key_view,
      [&key_view](auto& layer, auto) { return layer.end(); }, launch...);
}

template<class Location, class Key, class KeyView, class... Launch>
constexpr void buildLowerBoundLocation(Location& location, Key const& key,
                                       KeyView const& key_view,
                                       Launch... launch)
    FUNNEL_REQUIRES(is_key_view<KeyView>) {
  buildLocation(
      location, key_view,
      [&key, &key_view](auto& layer, auto) {
        return layer.lowerBound(key, key_view);
      },
      launch...);
}

template<class Location, class Key, class KeyView, class... Launch>
constexpr void buildUpperBoundLocation(Location& location, Key const& key,
                                       KeyView const& key_view,
                                       Launch... launch)
    FUNNEL_REQUIRES(is_key_view<KeyView>) {
  buildLocation(
      location, key_view,
      [&key, &key_view](auto& layer, auto) {
        return layer.upperBound(key, key_view);
      },
      launch...);
}

template<class Location, class KeyView, class... Launch>
constexpr void buildLocationAt(Location& location,
                               typename Location::Id target_id,
                               typename Location::Index target_index,
                               KeyView const& key_view, Launch... launch)
    FUNNEL_REQUIRES(is_key_view<KeyView>) {
  typename Location::Value& target_value{
      location.getLayer(target_id)[target_index]};
  buildLocation(
      location, key_view,
      [&key_view, target_id, target_index, &target_value](auto& layer,
                                                          auto id) {
        if (id == target_id) {
          return target_index;
        }
        if (id < target_id) {
          return layer.upperBound(target_value, key_view);
        }
        return layer.lowerBound(target_value, key_view);
      },
      launch...);
}

}  // namespace funnel::value_location
