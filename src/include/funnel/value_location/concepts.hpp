#pragma once

#include "../concepts.hpp"

#if FUNNEL_CONCEPTS

#include "../key_view/concepts.hpp"
#include "../layer/concepts.hpp"

namespace funnel {

template<class ValueLocation>
concept is_value_location = requires(
    ValueLocation value_location, typename ValueLocation::Index index,
    typename ValueLocation::Id id, typename ValueLocation::Value value,
    typename ValueLocation::Order order, typename ValueLocation::Layer layer,
    typename ValueLocation::size_type s,
    typename ValueLocation::LayerArrayPointer layer_array_pointer) {
  requires is_layer<typename ValueLocation::layer>;
  { s } -> std::convertible_to<typename ValueLocation::Id>;
  { id } -> std::convertible_to<typename ValueLocation::size_type>;
  { value_location.empty() } -> std::convertible_to<bool>;
  value_location.reset(layer_array_pointer);
  {
    value_location.getLayerArray()
    } -> std::convertible_to<typename ValueLocation::LayerArrayPointer>;
  {
    value_location.numLayers()
    } -> std::convertible_to<typename ValueLocation::Id>;
  {
    value_location.getLayer(id)
    } -> std::convertible_to<
        std::add_lvalue_reference_t<typename ValueLocation::Layer>>;
  { value_location.getId() } -> std::convertible_to<typename ValueLocation::Id>;
  {
    value_location.getIndex()
    } -> std::convertible_to<typename ValueLocation::Index>;
  {
    value_location.value()
    } -> std::convertible_to<typename ValueLocation::Value>;
  { value_location.isAtBegin() } -> std::convertible_to<bool>;
  { value_location.isPastEnd() } -> std::convertible_to<bool>;
  {
    value_location.getOrder()
    } -> std::convertible_to<typename ValueLocation::Order>;
};

template<class ValueLocation, class KeyView>
concept is_value_location_compatible_with_key_view =
    requires(ValueLocation value_location, KeyView key_view) {
  requires is_value_location<ValueLocation>;
  requires is_key_view<KeyView>;
  value_location.build(key_view);
  value_location.moveForward(key_view);
  value_location.moveBackward(key_view);
};

}  // namespace funnel

#endif
