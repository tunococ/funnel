#pragma once

#include "../layer_array/concepts.hpp"
#include "search_operations.hpp"

#include <cassert>
#include <cstddef>
#include <memory>
#include <optional>
#include <type_traits>
#include <utility>
#include <vector>

namespace funnel::value_location {

template<class AllocatorT = std::allocator<std::size_t>>
class BinaryHeap {
public:
  using Allocator = AllocatorT;
  using size_type = typename std::allocator_traits<AllocatorT>::size_type;
  using Id = size_type;
  using Index = size_type;

  constexpr BinaryHeap(
      size_type capacity = 0,
      Allocator const& allocator =
          Allocator()) noexcept(noexcept(std::vector<Id>(capacity, Id{},
                                                         allocator)))
      : index_2_id(capacity, Id{}, allocator),
        id_2_index(capacity, Index{}, allocator) {
    for (Index i{0}; i < capacity; ++i) {
      index_2_id[i] = i;
      id_2_index[i] = i;
    }
  }

  constexpr size_type size() const noexcept { return index_2_id.size(); }

  constexpr bool empty() const noexcept { return index_2_id.empty(); }

  constexpr void reset(size_type capacity = 0) {
    index_2_id.resize(capacity);
    id_2_index.resize(capacity);
    for (Index i{0}; i < capacity; ++i) {
      index_2_id[i] = i;
      id_2_index[i] = i;
    }
  }

  template<class Compare>
  constexpr void build(Compare const& compare) noexcept(
      std::is_nothrow_invocable_v<Compare, Id, Id>) {
    if (size() > 1) {
      for (Index i{size() / 2}; i > 0;) {
        --i;
        bubbleDown(i, compare);
      }
    }
  }

  template<class Compare>
  constexpr void increaseValue(Id id, Compare const& compare) noexcept(
      std::is_nothrow_invocable_v<Compare, Id, Id>) {
    assert(((id >= 0) && (id < size())) && "id out of range");
    bubbleDown(id_2_index[id], compare);
  }

  template<class Compare>
  constexpr void decreaseValue(Id id, Compare const& compare) noexcept(
      std::is_nothrow_invocable_v<Compare, Id, Id>) {
    assert(((id >= 0) && (id < size())) && "id out of range");
    bubbleUp(id_2_index[id], compare);
  }

  constexpr Id minId() const noexcept {
    assert((size() > 0) && "minId() cannot be called when size() is 0");
    return idAt(0);
  }

protected:
  constexpr Id idAt(Index index) const noexcept {
    assert(((index >= 0) && (index < size())) && "index out of range");
    return index_2_id[index];
  }

  constexpr Index indexOf(Id id) const noexcept {
    assert(((id >= 0) && (id < size())) && "id out of range");
    return id_2_index[id];
  }

  template<class Compare>
  constexpr void bubbleDown(Index index, Compare const& compare) noexcept(
      std::is_nothrow_invocable_v<Compare, Id, Id>) {
    assert(((index >= 0) && (index < size())) && "index out of range");
    for (Index child_index{index * 2 + 1}; child_index < size();
         child_index = index * 2 + 1) {
      Index right_child_index{child_index + 1};
      if ((right_child_index < size()) &&
          compare(index_2_id[right_child_index], index_2_id[child_index])) {
        child_index = right_child_index;
      }
      if (compare(index_2_id[index], index_2_id[child_index])) {
        break;
      }
      swap(index, child_index);
      index = child_index;
    }
  }

  template<class Compare>
  constexpr void bubbleUp(Index index, Compare const& compare) noexcept(
      std::is_nothrow_invocable_v<Compare, Id, Id>) {
    assert(((index >= 0) && (index < size())) && "index out of range");
    while (index > 0) {
      Index parent_index{(index - 1) / 2};
      if (compare(index_2_id[index], index_2_id[parent_index])) {
        swap(index, parent_index);
        index = parent_index;
        continue;
      }
      break;
    }
  }

  constexpr void swap(Index index_1, Index index_2) {
    Id& id_1{index_2_id[index_1]};
    Id& id_2{index_2_id[index_2]};
    std::swap(id_2_index[id_1], id_2_index[id_2]);
    std::swap(id_1, id_2);
  }

  std::vector<Id, Allocator> index_2_id;
  std::vector<Index, Allocator> id_2_index;

  // For testing.
  friend class BinaryHeapProbe;
};

/** @brief
 *  `layer_array`-based value locator.
 */
template<class LayerArrayPointerT,
         class AllocatorT = std::allocator<std::size_t>>
FUNNEL_REQUIRES(
    (is_layer_array<
        typename std::pointer_traits<LayerArrayPointerT>::element_type>))
class BinaryHeapValueLocation {
public:
  using LayerArrayPointer = LayerArrayPointerT;
  using LayerArray =
      typename std::pointer_traits<LayerArrayPointer>::element_type;
  using size_type = typename LayerArray::size_type;

  using This = BinaryHeapValueLocation<LayerArrayPointerT, AllocatorT>;
  using Allocator = typename std::allocator_traits<
      AllocatorT>::template rebind_alloc<size_type>;

  using Layer = typename LayerArray::value_type;
  using Index = typename Layer::Index;
  using Order = Index;
  using Value = typename Layer::Value;

  using Heap = BinaryHeap<Allocator>;
  using Id = typename Heap::Id;

  static_assert(std::is_same_v<size_type, Id>,
                "Id and size_type must be the same type");

  constexpr BinaryHeapValueLocation(
      LayerArrayPointer id_2_layer = nullptr,
      Allocator const& allocator = Allocator()) noexcept(noexcept(Heap{
      std::declval<Id>(), allocator}))
      : id_2_layer_{id_2_layer},
        fw_heap_(id_2_layer ? id_2_layer->size() : 0, allocator),
        bw_heap_(id_2_layer ? id_2_layer->size() : 0, allocator),
        id_2_index_pair_(id_2_layer ? id_2_layer->size() : 0, allocator) {}

  constexpr Id numLayers() const noexcept { return id_2_index_pair_.size(); }

  constexpr bool empty() const noexcept { return id_2_index_pair_.empty(); }

  constexpr void reset(LayerArrayPointer id_2_layer = nullptr) {
    id_2_layer_ = id_2_layer;
    size_type new_size{id_2_layer ? id_2_layer->size() : 0};
    fw_heap_.reset(new_size);
    bw_heap_.reset(new_size);
    id_2_index_pair_.resize(new_size);
  }

  constexpr LayerArrayPointer getLayerArray() const noexcept {
    return id_2_layer_;
  }

  constexpr Layer& getLayer(Id id) const noexcept {
    return id_2_layer_->layer(id);
  }

  constexpr Id getForwardId() const noexcept { return fw_heap_.minId(); }

  constexpr Id getBackwardId() const noexcept { return bw_heap_.minId(); }

  constexpr Index getForwardIndex() const noexcept {
    return id_2_index_pair_[getForwardId()].fw_index;
  }

  constexpr Index getBackwardIndex() const noexcept {
    assert((id_2_index_pair_[getBackwardIndex()].bw_dist > 0) &&
           "already past the begin location");
    return id_2_index_pair_[getBackwardIndex()].bwIndex();
  }

  constexpr Value& forwardValue() noexcept {
    assert((id_2_layer_) && "layer_array must not be null");
    Id id{fw_heap_.minId()};
    return id_2_layer_->layer(id)[id_2_index_pair_[id].fw_index];
  }

  constexpr Value& backwardValue() noexcept {
    assert((id_2_layer_) && "layer_array must not be null");
    Id id{bw_heap_.minId()};
    return id_2_layer_->layer(id)[id_2_index_pair_[id].bwIndex()];
  }

  constexpr Value& value() noexcept { return forwardValue(); }
  constexpr Value& prevValue() noexcept { return backwardValue(); }
  constexpr Id getId() const noexcept { return getForwardId(); }
  constexpr Index getIndex() const noexcept { return getForwardIndex(); }

  constexpr bool isAtBegin() const noexcept {
    return id_2_index_pair_[bw_heap_.minId()].bw_dist == 0;
  }

  constexpr bool isPastEnd() const noexcept {
    Id id{fw_heap_.minId()};
    Layer& layer{id_2_layer_->layer(id)};
    return id_2_index_pair_[id].fw_index >= layer.end();
  }

  template<class KeyView>
  constexpr void moveForward(KeyView const& key_view)
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    assert(!isPastEnd() && "cannot move forward past the end location");
    Id id{fw_heap_.minId()};
    IndexPair& index_pair{id_2_index_pair_[id]};
    Layer& layer{id_2_layer_->layer(id)};
    Index old_index{index_pair.fw_index};
    index_pair.fw_index = layer.next(old_index);
    index_pair.bw_dist = index_pair.fw_index - old_index;
    order_ += index_pair.bw_dist;
    fw_heap_.increaseValue(id, [&key_view, this](Id a, Id b) {
      return compareFw(a, b, key_view);
    });
    bw_heap_.decreaseValue(id, [&key_view, this](Id a, Id b) {
      return compareBw(a, b, key_view);
    });
  }

  template<class KeyView>
  constexpr void moveBackward(KeyView const& key_view)
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    assert(!isAtBegin() && "cannot move backward from the begin location");
    Id id{bw_heap_.minId()};
    IndexPair& index_pair{id_2_index_pair_[id]};
    Layer& layer{id_2_layer_->layer(id)};
    Index old_index{index_pair.bwIndex()};
    order_ -= index_pair.bw_dist;
    index_pair.fw_index = old_index;
    if (old_index == layer.frontIndex()) {
      index_pair.bw_dist = 0;
    } else {
      index_pair.bw_dist = old_index - layer.prev(old_index);
    }
    fw_heap_.decreaseValue(id, [&key_view, this](Id a, Id b) {
      return compareFw(a, b, key_view);
    });
    bw_heap_.increaseValue(id, [&key_view, this](Id a, Id b) {
      return compareBw(a, b, key_view);
    });
  }

  constexpr void setLayerIndex(Id id, Index index) noexcept {
    assert((id_2_layer_) && "layer_array must not be null");
    IndexPair& index_pair{id_2_index_pair_[id]};
    index_pair.fw_index = index;
    Layer& layer{id_2_layer_->layer(id)};
    if (!layer.empty() && layer.frontIndex() < index) {
      index_pair.bw_dist = index - layer.prev(index);
    } else {
      index_pair.bw_dist = 0;
    }
  }

  template<class KeyView>
  constexpr void build(KeyView const& key_view) noexcept(
      noexcept(key_view.compare(std::declval<Value>(), std::declval<Value>())))
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    order_ = 0;
    for (Id id{0}; id < numLayers(); ++id) {
      order_ += id_2_index_pair_[id].fw_index;
    }
    fw_heap_.build(
        [&key_view, this](Id a, Id b) { return compareFw(a, b, key_view); });
    bw_heap_.build(
        [&key_view, this](Id a, Id b) { return compareBw(a, b, key_view); });
  }

  template<class KeyView, class... Launch>
  constexpr void moveToBegin(KeyView const& key_view, Launch... launch)
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    buildBeginLocation(*this, key_view, launch...);
  }

  template<class KeyView, class... Launch>
  constexpr void moveToEnd(KeyView const& key_view, Launch... launch)
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    buildEndLocation(*this, key_view, launch...);
  }

  template<class Key, class KeyView, class... Launch>
  constexpr void moveToLowerBound(Key const& key, KeyView const& key_view,
                                  Launch... launch)
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    buildLowerBoundLocation(*this, key, key_view, launch...);
  }

  template<class Key, class KeyView, class... Launch>
  constexpr void moveToUpperBound(Key const& key, KeyView const& key_view,
                                  Launch... launch)
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    buildUpperBoundLocation(*this, key, key_view, launch...);
  }

  template<class KeyView, class... Launch>
  constexpr void moveTo(Id id, Index index, KeyView const& key_view,
                        Launch... launch)
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    buildLocationAt(*this, id, index, key_view, launch...);
  }

  constexpr Order getOrder() const noexcept { return order_; }

  constexpr bool operator==(const This& other) const noexcept {
    return order_ == other.order_;
  }
  constexpr bool operator!=(const This& other) const noexcept {
    return order_ != other.order_;
  }
#ifdef __cpp_impl_three_way_comparison
  constexpr auto operator<=>(const This& other) const noexcept {
    return order_ <=> other.order_;
  }
#else
  constexpr bool operator<(const This& other) const noexcept {
    return order_ < other.order_;
  }
  constexpr bool operator<=(const This& other) const noexcept {
    return order_ <= other.order_;
  }
  constexpr bool operator>(const This& other) const noexcept {
    return order_ > other.order_;
  }
  constexpr bool operator>=(const This& other) const noexcept {
    return order_ >= other.order_;
  }
#endif

protected:
  template<class KeyView>
  constexpr bool compareFw(Id a, Id b, KeyView const& key_view) noexcept(
      noexcept(key_view.compare(std::declval<Value>(), std::declval<Value>())))
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    Layer& layer_a{id_2_layer_->layer(a)};
    Layer& layer_b{id_2_layer_->layer(b)};
    Index index_a{id_2_index_pair_[a].fw_index};
    Index index_b{id_2_index_pair_[b].fw_index};
    if (index_a == layer_a.end()) {
      if (index_b == layer_b.end()) {
        return a < b;
      }
      return false;
    }
    if (index_b == layer_b.end()) {
      return true;
    }
    Value& value_a{layer_a[index_a]};
    Value& value_b{layer_b[index_b]};
    if (key_view.compare(value_a, value_b)) {
      return true;
    }
    if (key_view.compare(value_b, value_a)) {
      return false;
    }
    return a < b;
  }

  template<class KeyView>
  constexpr bool compareBw(Id a, Id b, KeyView const& key_view) noexcept(
      noexcept(key_view.compare(std::declval<Value>(), std::declval<Value>())))
      FUNNEL_REQUIRES(is_key_view<KeyView>) {
    Layer& layer_a{id_2_layer_->layer(a)};
    Layer& layer_b{id_2_layer_->layer(b)};
    IndexPair& index_pair_a{id_2_index_pair_[a]};
    IndexPair& index_pair_b{id_2_index_pair_[b]};
    if (index_pair_a.pastBegin()) {
      if (index_pair_b.pastBegin()) {
        return b < a;
      }
      return false;
    }
    if (index_pair_b.pastBegin()) {
      return true;
    }
    Value& value_a{layer_a[index_pair_a.bwIndex()]};
    Value& value_b{layer_b[index_pair_b.bwIndex()]};
    if (key_view.compare(value_b, value_a)) {
      return true;
    }
    if (key_view.compare(value_a, value_b)) {
      return false;
    }
    return b < a;
  }

  LayerArrayPointer id_2_layer_;
  Heap fw_heap_;
  Heap bw_heap_;

  struct IndexPair {
    Index fw_index;
    Index bw_dist;
    constexpr IndexPair(Index fw_index = 0, Index bw_dist = 0) noexcept
        : fw_index{fw_index}, bw_dist{bw_dist} {}
    constexpr Index bwIndex() const noexcept {
      assert((bw_dist > 0) && "bw_dist must be non-zero");
      return fw_index - bw_dist;
    }
    constexpr bool pastBegin() const noexcept { return bw_dist == 0; }
  };
  using IndexPairAllocator = typename std::allocator_traits<
      Allocator>::template rebind_alloc<IndexPair>;
  std::vector<IndexPair, IndexPairAllocator> id_2_index_pair_;

  Index order_;

  friend class BinaryHeapValueLocationProbe;
};

}  // namespace funnel::value_location
