#pragma once

#include <functional>
#include <utility>

namespace funnel::key_view {

template<class ValueT, class CompareT = std::less<ValueT>>
struct IdentityKeyView {
  using Value = ValueT;
  using Key = Value;
  using Mapped = Value;
  using KeyCompare = CompareT;

  KeyCompare key_compare{};

  static constexpr bool key_compare_noexcept{
      noexcept(key_compare(std::declval<Key>(), std::declval<Key>()))};

  constexpr IdentityKeyView(
      KeyCompare const& key_compare =
          KeyCompare()) noexcept(noexcept(KeyCompare(key_compare)))
      : key_compare(key_compare) {}

  constexpr Key& key(Value& value) const noexcept {
    return value;
  }

  constexpr Key const& key(Value const& value) const noexcept {
    return value;
  }

  constexpr Mapped& mapped(Value& value) const noexcept {
    return value;
  }

  constexpr Mapped const& mapped(Value const& value) const noexcept {
    return value;
  }

  constexpr bool compare(Key const& a, Key const& b) const
      noexcept(key_compare_noexcept) {
    return key_compare(a, b);
  }
};

}  // namespace funnel::key_view
