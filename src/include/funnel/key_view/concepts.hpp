#pragma once

#include "../concepts.hpp"

#if FUNNEL_CONCEPTS

namespace funnel {

template<class View>
concept is_key_view = requires(View view, typename View::Key key,
                               typename View::Value value) {
  typename View::Mapped;
  { view.key(value) } -> std::convertible_to<typename View::Key const&>;
  {
    view.key(const_cast<typename View::Value const&>(value))
    } -> std::convertible_to<typename View::Key const&>;
  { view.mapped(value) } -> std::convertible_to<typename View::Mapped&>;
  {
    view.mapped(const_cast<typename View::Value const&>(value))
    } -> std::convertible_to<typename View::Mapped const&>;
  { view.compare(key, key) } -> std::convertible_to<bool>;
  { view.compare(value, key) } -> std::convertible_to<bool>;
  { view.compare(key, value) } -> std::convertible_to<bool>;
  { view.compare(value, value) } -> std::convertible_to<bool>;
};

}  // namespace funnel

#endif
