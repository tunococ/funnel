#pragma once

#include <functional>
#include <utility>

namespace funnel::key_view {

template<class KeyT, class MappedT, class KeyCompareT = std::less<KeyT>>
struct PairKeyView {
  using Key = KeyT;
  using Mapped = MappedT;
  using Value = std::pair<Key, Mapped>;
  using KeyCompare = KeyCompareT;

  KeyCompare key_compare{};

  static constexpr bool key_compare_noexcept{
      noexcept(key_compare(std::declval<Key>(), std::declval<Key>()))};

  constexpr PairKeyView(KeyCompare const& key_compare = KeyCompare()) noexcept(
      noexcept(KeyCompare(key_compare)))
      : key_compare(key_compare) {}

  constexpr Key& key(Value& value) const noexcept { return value.first; }

  constexpr Key const& key(Value const& value) const noexcept {
    return value.first;
  }

  constexpr Mapped& mapped(Value& value) const noexcept { return value.second; }

  constexpr Mapped const& mapped(Value const& value) const noexcept {
    return value.second;
  }

  constexpr bool compare(Key const& a, Key const& b) const
      noexcept(key_compare_noexcept) {
    return key_compare(a, b);
  }

  constexpr bool compare(Value const& a, Key const& b) const
      noexcept(key_compare_noexcept) {
    return key_compare(a.first, b);
  }

  constexpr bool compare(Key const& a, Value const& b) const
      noexcept(key_compare_noexcept) {
    return key_compare(a, b.first);
  }

  constexpr bool compare(Value const& a, Value const& b) const
      noexcept(key_compare_noexcept) {
    return key_compare(a.first, b.first);
  }
};

}  // namespace funnel::key_view
