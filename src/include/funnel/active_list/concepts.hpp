#pragma once

/** @file
 *  @brief ActiveList
 *
 *  An *ActiveList* is a data structure abstraction for *layers* in a funnel.
 *  It must support the following operations efficiently:
 *  - insertion at an end (back only, or both back and front);
 *  - deactivation (lazy deletion) at any index;
 *  - element access (value and active state) at any index;
 *  - iterating through active indices forward and backward.
 *
 *  If an insertion is only allowed on the back, the list is said to be
 *  *one-sided*; it is *two-sided* otherwise.
 *  The funnel needs to know if the list is one-sided or two-sided in order to
 *  operate correctly and efficiently.
 */

#include "../concepts.hpp"
#include "../key_view/concepts.hpp"

#if FUNNEL_CONCEPTS

#include <compare>
#include <utility>

namespace funnel {

template<class List>
concept is_active_list = requires(List list, typename List::Allocator allocator,
                                  typename List::Index index, int i) {
  typename List::Allocator;
  typename List::Index;
  typename List::size_type;
  typename List::Value;
  List{};
  List{allocator};
  List{std::move(list)};
  list = std::move(list);
  { List::is_two_sided } -> std::convertible_to<bool>;
  {list.clear()};
  { list.getAllocator() } -> std::convertible_to<typename List::Allocator>;
  { list.numActive() } -> std::convertible_to<typename List::size_type>;
  { list.capacity() } -> std::convertible_to<typename List::size_type>;
  { list.beginIndex() } -> std::convertible_to<typename List::Index>;
  { list.endIndex() } -> std::convertible_to<typename List::Index>;
  { list.beginActiveIndex() } -> std::convertible_to<typename List::Index>;
  { list.endActiveIndex() } -> std::convertible_to<typename List::Index>;
  { list.nextActiveIndex(index) } -> std::convertible_to<typename List::Index>;
  { list.prevActiveIndex(index) } -> std::convertible_to<typename List::Index>;
  list.deactivate(index);
  { list.isActive(index) } -> std::convertible_to<bool>;
  { i } -> std::convertible_to<typename List::Index>;
  { index <=> index } -> std::convertible_to<std::strong_ordering>;
  { index + i } -> std::convertible_to<typename List::Index>;
  { index - i } -> std::convertible_to<typename List::Index>;
  { index + index } -> std::convertible_to<typename List::Index>;
  { index - index } -> std::convertible_to<typename List::Index>;
  { index / i } -> std::convertible_to<typename List::Index>;
};

template<class List>
concept is_active_list_with_values = requires(List list,
                                              typename List::Index index) {
  requires is_active_list<List>;
  { list.valueAt(index) } -> std::convertible_to<typename List::Value>;
};

}  // namespace funnel

#endif
