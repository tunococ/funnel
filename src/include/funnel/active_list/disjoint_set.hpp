#pragma once

/** @file
 *
 *  @brief
 *  Implementation of `ActiveList` that uses the disjoint set data structure to
 *  keep track of intervals of inactive entries.
 */

#include "../tracker/index.hpp"
#include "concepts.hpp"

#include <cassert>
#include <cstddef>
#include <deque>
#include <functional>
#include <memory>
#include <type_traits>
#include <variant>
#include <vector>

namespace funnel::active_list {

using funnel::tracker::rawPointer;
using funnel::tracker::TrackedIndices;
using funnel::tracker::trackIndices;

template<class ValueT = void, class SizeT = std::size_t>
class DisjointSetEntry;

template<class SizeT>
struct DisjointSetEntry<void, SizeT> {
  using This = DisjointSetEntry<void, SizeT>;
  using size_type = SizeT;
  using ssize_type = std::make_signed_t<size_type>;
  using Value = void;
  static constexpr bool has_value{false};

  struct RootState {
    size_type left{0};
    size_type right{0};
    size_type rank{1};
    constexpr RootState() noexcept = default;
    constexpr RootState(size_type left, size_type right,
                        size_type rank) noexcept
        : left{left}, right{right}, rank{rank} {}
  };

  using ChildState = ssize_type;

  enum StateIndex : std::size_t {
    ACTIVE = 0,
    ROOT = 1,
    CHILD = 2,
  };

  using State = std::variant<std::monostate, RootState, ChildState>;

  State state{std::in_place_index<ACTIVE>};

  constexpr bool isActive() const noexcept { return state.index() == ACTIVE; }
  constexpr bool isRoot() const noexcept { return state.index() == ROOT; }
  constexpr bool isChild() const noexcept { return state.index() == CHILD; }

  constexpr size_type& leftSpan() noexcept {
    return std::get<ROOT>(state).left;
  }
  constexpr size_type& rightSpan() noexcept {
    return std::get<ROOT>(state).right;
  }
  constexpr size_type& rootRank() noexcept {
    return std::get<ROOT>(state).rank;
  }
  constexpr ssize_type& parentOffset() noexcept {
    return std::get<CHILD>(state);
  }

  constexpr void makeRoot(size_type left, size_type right,
                          size_type rank) noexcept {
    state.template emplace<ROOT>(left, right, rank);
  }
  constexpr void makeChild(ssize_type parent_offset) noexcept {
    state.template emplace<CHILD>(parent_offset);
  }

  constexpr static This construct() noexcept { return This{}; }
};

template<class ValueT, class SizeT>
struct DisjointSetEntry : DisjointSetEntry<void, SizeT> {
  using This = DisjointSetEntry<ValueT, SizeT>;
  using Super = DisjointSetEntry<void, SizeT>;
  using size_type = SizeT;
  using ssize_type = std::make_signed_t<size_type>;
  using Value = ValueT;
  static constexpr bool has_value{true};

  Value value;

  constexpr DisjointSetEntry() = default;  // LCOV_EXCL_LINE
  constexpr DisjointSetEntry(This&&) = default;
  constexpr DisjointSetEntry(This const&) = default;
  This& operator=(This&&) = default;
  This& operator=(This const&) = default;

private:
  struct MakeValue {};

  template<class... Args>
  constexpr DisjointSetEntry(MakeValue, Args&&... args) noexcept(
      std::is_nothrow_constructible_v<Value, Args...>)
      : value(std::forward<Args>(args)...) {}

public:
  template<class... Args>
  constexpr static This construct(Args&&... args) noexcept(
      std::is_nothrow_constructible_v<Value, Args...>) {
    return DisjointSetEntry{MakeValue{}, std::forward<Args>(args)...};
  }
};

template<class ValueT, class AllocatorT, template<class, class> class BaseListF>
class DisjointSetActiveList {
public:
  using This = DisjointSetActiveList<ValueT, AllocatorT, BaseListF>;
  using Value = ValueT;
  using size_type = typename AllocatorT::size_type;
  using Entry = DisjointSetEntry<Value, size_type>;
  using Allocator =
      typename std::allocator_traits<AllocatorT>::template rebind_alloc<Entry>;
  using EntryList = BaseListF<Entry, Allocator>;
  static_assert(std::is_same_v<Value, typename Entry::Value>,
                "Value and Entry::Value must match");
  static_assert(
      std::is_same_v<typename EntryList::size_type, typename Entry::size_type>,
      "EntryList::size_type and Entry::size_type must match");

  using Index = size_type;

  static constexpr bool entries_have_values{Entry::has_value};

private:
  FUNNEL_DEFINE_HAS_MEMBER_FUNCTION(emplace_front)

public:
  static constexpr bool is_two_sided{
      _has_member_function_emplace_front<EntryList, Entry>};

  constexpr DisjointSetActiveList(
      Allocator const& allocator =
          Allocator()) noexcept(noexcept(EntryList(allocator)))
      : entries_(allocator) {}

  constexpr DisjointSetActiveList(This&&) = default;
  constexpr DisjointSetActiveList(This const&) = default;
  constexpr This& operator=(This&&) = default;
  constexpr This& operator=(This const&) = default;

  constexpr void clear() noexcept(noexcept(std::declval<EntryList>().clear())) {
    entries_.clear();
    num_active_ = 0;
  }

  constexpr Allocator getAllocator() const noexcept {
    return entries_.get_allocator();
  }

  template<class... Args>
  constexpr Index emplaceFront(Args&&... args)
      FUNNEL_REQUIRES(requires(EntryList entries, Args... args) {
        entries.emplace_front(Entry::construct(args...));
      }) {
    entries_.emplace_front(Entry::construct(std::forward<Args>(args)...));
    front_active_index_ = 0;
    ++num_active_;
    if (num_active_ == 1) {
      back_active_index_ = 0;
    } else {
      ++back_active_index_;
    }
    return 0;
  }

  template<class... Args>
  constexpr Index emplaceBack(Args&&... args)
      FUNNEL_REQUIRES(requires(EntryList entries, Args... args) {
        entries.emplace_back(Entry::construct(args...));
      }) {
    Index const new_index{entries_.size()};
    entries_.emplace_back(Entry::construct(std::forward<Args>(args)...));
    back_active_index_ = new_index;
    ++num_active_;
    if (num_active_ == 1) {
      front_active_index_ = new_index;
    }
    return new_index;
  }

  constexpr Index numActive() const noexcept { return num_active_; }

  constexpr Index capacity() const noexcept { return entries_.size(); }

  constexpr bool isActive(Index index) const noexcept {
    return entries_[index].isActive();
  }

  constexpr Entry& entryAt(Index index) noexcept { return entries_[index]; }

  constexpr Entry const& entryAt(Index index) const noexcept {
    return entries_[index];
  }

  constexpr std::add_lvalue_reference_t<Value> valueAt(Index index) noexcept
      FUNNEL_REQUIRES(entries_have_values) {
    if constexpr (entries_have_values) {
      return entryAt(index).value;
    } else {
      assert(false && "valueAt() is not available for void values");
      throw;
    }
  }

  constexpr std::add_lvalue_reference_t<Value const> valueAt(
      Index index) const noexcept FUNNEL_REQUIRES(entries_have_values) {
    if constexpr (entries_have_values) {
      return entryAt(index).value;
    } else {
      assert(false && "valueAt() is not available for void values");
      throw;
    }
  }

  constexpr void deactivate(Index index) noexcept {
    assert(entryAt(index).isActive() && "index must point to an active entry");
    assert((num_active_ > 0) && "num_active_ must be positive");

    --num_active_;
    Entry& entry{entryAt(index)};

    const auto joinLeft = [&entry, index, this ]() constexpr {
      Index left_root_index{findRootIndex(index - 1)};
      entry.makeChild(left_root_index - index);
      ++entryAt(left_root_index).rightSpan();
      if (index == front_active_index_) {
        ++front_active_index_;
      } else if (index == back_active_index_ && num_active_ > 0) {
        back_active_index_ =
            left_root_index - entryAt(left_root_index).leftSpan() - 1;
      }
    };
    const auto joinRight = [&entry, index, this ]() constexpr {
      Index right_root_index{findRootIndex(index + 1)};
      entry.makeChild(right_root_index - index);
      ++entryAt(right_root_index).leftSpan();
      if (index == front_active_index_) {
        front_active_index_ =
            right_root_index + entryAt(right_root_index).rightSpan();
      } else if (index == back_active_index_ && num_active_ > 0) {
        --back_active_index_;
      }
    };

    // If `entry` is the leftmost entry.
    if (index == 0) {
      // If `entry` is isolated.
      if (entries_.size() == 1 || entryAt(index + 1).isActive()) {
        // Make it a new root.
        entry.makeRoot(0, 1, 0);
        // Update cached index.
        ++front_active_index_;
        return;
      }
      // Otherwise, join with inactive entries on the right.
      joinRight();
      return;
    }

    // If `entry` is the rightmost entry.
    if (index == entries_.size() - 1) {
      // If `entry` is isolated.
      if (entryAt(index - 1).isActive()) {
        // Make it a new root.
        entry.makeRoot(0, 1, 0);
        // Update cached index.
        --back_active_index_;
        return;
      }
      // Otherwise, join with inactive entries on the left.
      joinLeft();
      return;
    }

    // Now `entry` is somewhere in the middle.

    // If the entry on the right of `entry` is active.
    if (entryAt(index + 1).isActive()) {
      // If `entry` is isolated.
      if (entryAt(index - 1).isActive()) {
        // Make it a new root.
        entry.makeRoot(0, 1, 0);
        return;
      }
      // Otherwise, join with inactive entries_ on the left.
      joinLeft();
      return;
    }

    // If the entry on the left of `entry` is active.
    if (entryAt(index - 1).isActive()) {
      // Join with inactive entries_ on the right.
      joinRight();
      return;
    }

    // Merge left and right intervals of inactive entries_.

    Index left_root_index{findRootIndex(index - 1)};
    Entry& left_root_entry{entryAt(left_root_index)};

    Index right_root_index{findRootIndex(index + 1)};
    Entry& right_root_entry{entryAt(right_root_index)};

    // If the left root has a lower rank than the right root.
    if (left_root_entry.rootRank() < right_root_entry.rootRank()) {
      // Make the left root a child of the right root.
      right_root_entry.leftSpan() +=
          left_root_entry.leftSpan() + left_root_entry.rightSpan() + 1;
      left_root_entry.makeChild(right_root_index - left_root_index);
      entry.makeChild(right_root_index - index);
      // Update cached index.
      if (index == front_active_index_) {
        front_active_index_ = right_root_index + right_root_entry.rightSpan();
      } else if (index == back_active_index_ && num_active_ > 0) {
        back_active_index_ = right_root_index - right_root_entry.leftSpan() - 1;
      }
      return;
    }
    // If both roots have an equal rank.
    if (left_root_entry.rootRank() == right_root_entry.rootRank()) {
      // Promote the left root.
      ++left_root_entry.rootRank();
    }

    // Now, make the right root a child of the left root.
    left_root_entry.rightSpan() +=
        right_root_entry.leftSpan() + right_root_entry.rightSpan() + 1;
    right_root_entry.makeChild(left_root_index - right_root_index);
    entry.makeChild(left_root_index - index);
    // Update cached index.
    if (index == front_active_index_) {
      front_active_index_ = left_root_index + left_root_entry.rightSpan();
    } else if (index == back_active_index_ && num_active_ > 0) {
      back_active_index_ = left_root_index - left_root_entry.leftSpan() - 1;
    }
  }

  constexpr Index beginIndex() const noexcept { return 0; }

  constexpr Index endIndex() const noexcept { return entries_.size(); }

  constexpr Index frontActiveIndex() noexcept {
    assert((num_active_ > 0) &&
           "frontActiveIndex() is meaningless without active entries");
    return front_active_index_;
  }

  constexpr Index backActiveIndex() noexcept {
    assert((num_active_ > 0) &&
           "backActiveIndex() is meaningless without active entries");
    return back_active_index_;
  }

  constexpr Index beginActiveIndex() noexcept {
    if (num_active_ == 0) {
      return entries_.size();
    }
    assert((entries_.size() > 0) &&
           "entries_ must not be empty when num_active_ > 0");
    if (entryAt(0).isActive()) {
      return 0;
    }
    return rightActiveBeginIndex(0);
  }

  constexpr Index endActiveIndex() noexcept { return entries_.size(); }

  constexpr Index nextActiveIndex(Index index) noexcept {
    assert((index < endActiveIndex()) &&
           "index must be lower than endActiveIndex()");
    assert(entryAt(index).isActive() && "index must point to an active entry");
    if (index + 1 >= entries_.size() || entryAt(index + 1).isActive()) {
      return index + 1;
    }
    return rightActiveBeginIndex(index + 1);
  }

  constexpr Index prevActiveIndex(Index index) noexcept {
    assert((index > beginActiveIndex()) &&
           "index must exceed beginActiveIndex()");
    assert(((index == endActiveIndex()) || (entryAt(index).isActive())) &&
           "index must point to an active entry or equal endActiveIndex()");
    if (entryAt(index - 1).isActive()) {
      return index - 1;
    }
    assert((leftActiveEndIndex(index - 1) >= beginActiveIndex()) &&
           "index is already at the beginning");
    return leftActiveEndIndex(index - 1) - 1;
  }

protected:
  constexpr Index findRootIndex(Index index) noexcept {
    assert(!entryAt(index).isActive() &&
           "index must point to an inactive entry");
    while (true) {
      Entry& entry{entryAt(index)};
      if (entry.isRoot()) {
        return index;
      }
      assert(entry.isChild());
      Index const parent_index{index + entry.parentOffset()};
      Entry& parent_entry{entryAt(parent_index)};
      // Perform 1-step path compression if `parent_entry` is not a root.
      if (parent_entry.isChild()) {
        Index const new_parent_index{parent_index +
                                     parent_entry.parentOffset()};
        entry.makeChild(new_parent_index - index);
        index = new_parent_index;
      } else {
        index = parent_index;
      }
    }
  }

  constexpr Index rightActiveBeginIndex(Index index) noexcept {
    assert(!entryAt(index).isActive() &&
           "index must point to an inactive entry");
    Index root_index{findRootIndex(index)};
    Entry& root_entry{entryAt(root_index)};
    return root_index + root_entry.rightSpan();
  }

  constexpr Index leftActiveEndIndex(Index index) noexcept {
    assert(!entryAt(index).isActive() &&
           "index must point to an inactive entry");
    Index root_index{findRootIndex(index)};
    Entry& root_entry{entryAt(root_index)};
    return root_index - root_entry.leftSpan();
  }

  // Optimize

  /** @brief
   *  Sets all indices in @p tracked_indices whose values are greater than or
   *  equal to @p src to `endActiveIndex()`.
   *
   *  @param src Lower bound of indices in @p tracked_indices that will be
   *    moved to `endActiveIndex()`.
   *  @param[in,out] tracked_indices Indices to track. This must not be null.
   */
  template<std::size_t N>
  constexpr void trackEndIndices(
      Index src, TrackedIndices<Index, N>* tracked_indices) noexcept {
    if constexpr (N > 0) {
      if (tracked_indices->head >= src) {
        tracked_indices->head = endActiveIndex();
      }
      trackEndIndices(src, tracked_indices->tailPointer());
    }
  }

  /** @brief
   *  Contracts the array by moving active entries to inactive indices, starting
   *  from `src` as the first active index and `dst` as the first inactive
   *  index, while tracking indices in `tracked_indices`.
   *
   *  Values in `tracked_indices` that point to active entries before calling
   *  this function will be modified to point to the same entries after this
   *  function returns.
   *
   *  @param src Starting active index to move from.
   *  @param dst Starting inactive index to move to.
   *  @param[in,out] tracked_indices Indices to track. This must not be null.
   */
  template<std::size_t N>
  constexpr void contractAfterBeginning(
      Index src, Index dst,
      TrackedIndices<Index, N>* tracked_indices) noexcept {
    while (src < endIndex()) {
      assert(isActive(src) && "src must point to an active entry");
      Index next_src{nextActiveIndex(src)};
      entries_[dst] = std::move(entries_[src]);
      if constexpr (N > 0) {
        if (tracked_indices->head <= src) {
          tracked_indices->head = dst;
          return contractAfterBeginning(next_src, dst + 1,
                                        tracked_indices->tailPointer());
        }
      }
      src = next_src;
      ++dst;
    }
    entries_.resize(dst);
    front_active_index_ = 0;
    back_active_index_ = dst - 1;
    trackEndIndices(src, tracked_indices);
  }

  /** @brief
   *  Tries to find the first inactive index starting from @p index while
   *  tracking indices in @p tracked_indices , then calls
   *  `contractAfterBeginning(src, dst, tracked_indices)` where
   *  `dst` is the first inactive index, `src` is the next active index, and
   *  `tracked_indices` contains the indices from the original
   *  `tracked_indices` that have not been encountered.
   *
   *  If all indices are active, `contractAfterBeginning()` will not be called.
   *
   *  @param index Starting index to search.
   *  @param[in,out] tracked_indices Indices to track.
   */
  template<std::size_t N>
  constexpr void contract(Index index,
                          TrackedIndices<Index, N>* tracked_indices) noexcept {
    for (; true; ++index) {
      assert((index < endIndex()) &&
             "This function should have returned before this condition is "
             "reached");
      if (!isActive(index)) {
        return contractAfterBeginning(rightActiveBeginIndex(index), index,
                                      tracked_indices);
      }
      if constexpr (N > 0) {
        if (tracked_indices->head <= index) {
          return contract(index + 1, tracked_indices->tailPointer());
        }
      }
    }
  }

  /** @brief
   *  Removes all inactive entries while maintaining the order of active
   *  entries. Indices of entries moved by this operation can be tracked by
   *  supplying them in `tracked_indices`.
   *
   *  @param[in,out] tracked_indices Indices to track. This must not be null.
   */
  template<std::size_t N>
  constexpr void optimize_internal(
      TrackedIndices<Index, N>* tracked_indices) noexcept {
    if (num_active_ == 0) {
      entries_.resize(0);
      front_active_index_ = 0;
      back_active_index_ = 0;
      trackEndIndices(0, tracked_indices);
      return;
    }
    if (num_active_ == entries_.size()) {
      trackEndIndices(num_active_, tracked_indices);
      return;
    }
    contract(0, tracked_indices);
  }

public:
  /** @brief
   *  Removes all inactive entries while maintaining the order of active
   *  entries. Indices of entries moved by this operation can be tracked by
   *  supplying them in `tracked_indices`.
   *
   *  @param[in,out] tracked_indices Indices to track. This should be a pointer
   *    to `TrackedIndices<Index, N>` for some `N`.
   *    `funnel::tracker::trackIndices()` can be used to create this parameter.
   */
  template<class TrackedIndicesT>
  constexpr void optimize(TrackedIndicesT&& tracked_indices) noexcept {
    optimize_internal(rawPointer(tracked_indices));
  }

  /** @brief
   *  Removes all inactive entries while maintaining the order of active
   *  entries.
   */
  constexpr void optimize(std::nullptr_t = nullptr) noexcept {
    TrackedIndices<Index, 0> trackNothing;
    optimize_internal(&trackNothing);
  }

protected:
  EntryList entries_;
  size_type num_active_{0};
  Index front_active_index_{0};
  Index back_active_index_{0};

  // For testing.
  friend class DisjointSetActiveListProbe;
};

template<class Value, class Allocator>
using DisjointSetActiveDeque =
    DisjointSetActiveList<Value, Allocator, std::deque>;

template<class Value, class Allocator>
using DisjointSetActiveVector =
    DisjointSetActiveList<Value, Allocator, std::vector>;

}  // namespace funnel::active_list
