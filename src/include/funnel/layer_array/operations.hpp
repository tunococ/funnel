#pragma once

#include "../key_view/concepts.hpp"
#include "../layer/merger.hpp"
#include "../layer_array/concepts.hpp"
#include "concepts.hpp"

#include <atomic>
#include <cassert>
#include <cstddef>
#include <future>
#include <memory>
#include <utility>
#include <vector>

namespace funnel::layer_array {

using funnel::merger::TrackedIndices;
using funnel::merger::trackIndices;

template<class LayerArray>
using IdIndexPair =
    std::pair<typename LayerArray::Id, typename LayerArray::Index>;

/** @brief
 *  Finds an id-index pair of a value matching the given key.
 *  If no values match the key, the returned id-index pair will have its id
 *  equal to `layer_array.numLayers()`, which is an invalid id.
 *  If there are many values that match the key, the id-index pair of any of
 *  them may be returned.
 */
template<class LayerArray, class Key, class KeyView>
constexpr IdIndexPair<LayerArray> find(LayerArray& layer_array, Key const& key,
                                       KeyView const& key_view) noexcept
    FUNNEL_REQUIRES(is_layer_array<LayerArray>&& is_key_view<KeyView>) {
  using Id = typename LayerArray::Id;
  for (Id id{0}; id < layer_array.numLayers(); ++id) {
    auto& layer{layer_array.layer(id)};
    auto index{layer.find(key, key_view)};
    if (index != layer.end()) {
      return {id, index};
    }
  }
  return {layer_array.numLayers(), 0};
}

/** @brief
 *  Finds an id-index pair of a value matching the given key.
 *  If no values match the key, the returned id-index pair will have its id
 *  equal to `layer_array.numLayers()`, which is an invalid id.
 *  If there are many values that match the key, the id-index pair of any of
 *  them may be returned.
 *
 *  Since the search operation in each layer is independent from other layers,
 *  parallelization is possible if the supplied execution policy permits.
 *  Note that in the case where multiple values match the given key, the
 *  behavior of this function can be non-deterministic, i.e., calling it
 *  multiple times with the exact same input arguments might not yield the same
 *  output.
 */
template<class LayerArray, class Key, class KeyView,
         template<class, class> class TaskArrayF = std::vector,
         template<class> class TaskAllocatorF = std::allocator>
constexpr IdIndexPair<LayerArray> find(LayerArray& layer_array, Key const& key,
                                       KeyView const& key_view,
                                       std::launch policy)
    FUNNEL_REQUIRES(is_layer_array<LayerArray>&& is_key_view<KeyView>) {
  using Id = typename LayerArray::Id;
  using Result = IdIndexPair<LayerArray>;

  using Task = std::future<void>;
  using TaskAllocator = AllocatorF<Task>;
  using TaskArray = TaskArrayF<Task, TaskAllocator>;

  TaskArray tasks(layer_array.numLayers());
  std::atomic_bool not_found{true};
  std::atomic<Result> result{Result{layer_array.numLayers(), 0}};

  auto keep_searching = [&not_found]() {
    return not_found.load(std::memory_order_relaxed);
  };
  auto store_result = [&not_found, &result](Result const& found_result) {
    not_found.store(false, std::memory_order_relaxed);
    result.store(found_result, std::memory_order_relaxed);
  };
  auto search = [&layer_array, &key, &key_view, &keep_searching,
                 &store_result](Id id) {
    auto& layer{layer_array.layer(id)};
    auto index{layer.find(key, key_view, keep_searching)};
    if (index != layer.end()) {
      store_result(Result{id, index});
    }
  };

  for (Id id{0}; id < layer_array.numLayers(); ++id) {
    tasks[static_cast<std::size_t>(id)] = std::async(search, id);
  }
  for (auto& task : tasks) {
    task.wait();
  }
  return result.load(std::memory_order_relaxed);
}

/** @brief
 *  Returns `true` if and only if `layer_array` contains a value that matches
 *  the given key.
 */
template<class LayerArray, class Key, class KeyView, class... Launch>
constexpr bool contains(LayerArray& layer_array, Key const& key,
                        KeyView const& key_view, Launch... policy) noexcept
    FUNNEL_REQUIRES(is_layer_array<LayerArray>&& is_key_view<KeyView>) {
  return find(layer_array, key, key_view, policy...).first !=
         layer_array.numLayers();
}

/** @brief
 *  Erases a value at the given `index` in the layer with the given `id`.
 *
 *  This function also returns the index of the next element in the same layer.
 */
template<class LayerArray>
constexpr typename LayerArray::Index erase(
    LayerArray& layer_array, typename LayerArray::Id id,
    typename LayerArray::Index index) noexcept
    FUNNEL_REQUIRES(is_layer_array<LayerArray>) {
  return layer_array.layer(id).erase(index);
}

/** @brief
 *  Erases a value at the given id-index pair.
 *
 *  This function also returns the index of the next element in the same layer.
 */
template<class LayerArray>
constexpr typename LayerArray::Index erase(
    LayerArray& layer_array,
    IdIndexPair<LayerArray> const& id_index_pair) noexcept
    FUNNEL_REQUIRES(is_layer_array<LayerArray>) {
  return erase(layer_array, id_index_pair.first, id_index_pair.second);
}

/** @brief
 *  Constructs a value and adds it to the back of one of the layers, or to a
 *  newly-created layer, while maintaining the non-decreasing order of each
 *  layer in `layer_array`. The return value is the id of the *first* layer
 *  that the new value gets added to, i.e., it is the lowest possible layer id
 *  that allows adding the value without violating the order.
 *
 *  This function assumes that `key` will be the first argument and
 *  `mapped_args...` will be the remaining arguments of the constructor of
 *  `Value`. (`mapped_args...` can be empty.) `key` must already be
 *  constructed because it is needed in comparisons.
 *
 *  This function does not check whether `key` exists in `layer_array`. It
 *  always adds a value to `layer_array`, hence allowing duplicates to exist.
 */
template<class LayerArray, class Key, class KeyView, class... MappedArgs>
constexpr typename LayerArray::Id emplaceBack(LayerArray& layer_array,
                                              Key&& key,
                                              KeyView const& key_view,
                                              MappedArgs&&... mapped_args)
    FUNNEL_REQUIRES(is_layer_array<LayerArray>&& is_key_view<KeyView>) {
  using Id = typename LayerArray::Id;
  for (Id id{0}; id < layer_array.numLayers(); ++id) {
    auto& layer{layer_array.layer(id)};
    if (layer.empty() || !key_view.compare(key, layer.back())) {
      layer.emplaceBack(std::forward<Key>(key),
                        std::forward<MappedArgs>(mapped_args)...);
      return id;
    }
  }
  layer_array.newLayer().emplaceBack(std::forward<Key>(key),
                                     std::forward<MappedArgs>(mapped_args));
  return layer_array.numLayers() - 1;
}

/** @brief
 *  Constructs a value and adds it to the front of one of the layers, or to a
 *  newly-created layer, while maintaining the non-decreasing order of each
 *  layer in `layer_array`. The return value is the id of the *first* layer
 *  that the new value gets added to, i.e., it is the lowest possible layer id
 *  that allows adding the value without violating the order.
 *
 *  This function assumes that `key` will be the first argument and
 *  `mapped_args...` will be the remaining arguments of the constructor of
 *  `Value`. (`mapped_args...` can be empty.) `key` must already be
 *  constructed because it is needed in comparisons.
 *
 *  This function does not check whether `key` exists in `layer_array`. It
 *  always adds a value to `layer_array`, hence allowing duplicates to exist.
 */
template<class LayerArray, class Key, class KeyView, class... MappedArgs>
constexpr typename LayerArray::Id emplaceFront(LayerArray& layer_array,
                                               Key&& key,
                                               KeyView const& key_view,
                                               MappedArgs&&... mapped_args)
    FUNNEL_REQUIRES(is_layer_array<LayerArray>&& is_key_view<KeyView>&&
                        LayerArray::Layer::is_two_sided) {
  using Id = typename LayerArray::Id;
  for (Id id{0}; id < layer_array.numLayers(); ++id) {
    auto& layer{layer_array.layer(id)};
    if (layer.empty() || !key_view.compare(layer.front(), key)) {
      layer.emplaceFront(std::forward<Key>(key),
                         std::forward<MappedArgs>(mapped_args)...);
      return id;
    }
  }
  layer_array.newLayer().emplaceFront(std::forward<Key>(key),
                                      std::forward<MappedArgs>(mapped_args));
  return layer_array.numLayers() - 1;
}

template<class LayerArray, class KeyView, class TrackedIndices1,
         class TrackedIndices2>
constexpr void merge(LayerArray& layer_array, typename LayerArray::Id id_1,
                     typename LayerArray::Id id_2,
                     TrackedIndices1&& tracked_indices_1,
                     TrackedIndices2&& tracked_indices_2,
                     KeyView const& key_view)
    FUNNEL_REQUIRES(is_layer_array<LayerArray>&& is_key_view<KeyView>) {
  layer_array.layer(id_1) =
      funnel::layer::merge(layer_array.layer(id_1), layer_array.layer(id_2),
                           tracked_indices_1, tracked_indices_2, key_view);
  layer_array.deleteLayer(id_2);
}

template<class LayerArray, class KeyView>
constexpr void merge(LayerArray& layer_array, typename LayerArray::Id id_1,
                     typename LayerArray::Id id_2, KeyView const& key_view)
    FUNNEL_REQUIRES(is_layer_array<LayerArray>&& is_key_view<KeyView>) {
  merge(layer_array, id_1, id_2, trackIndices(), trackIndices(), key_view);
}

namespace tracker {

template<class Function, class IndexPointerListIterator, class TrackedIndices1,
         class TrackedIndices2>
constexpr void callAndTrack2(Function&& f, IndexPointerListIterator begin_1,
                             IndexPointerListIterator end_1,
                             TrackedIndices1&& tracked_indices_1,
                             IndexPointerListIterator begin_2,
                             IndexPointerListIterator end_2,
                             TrackedIndices2&& tracked_indices_2) {}

template<class Function, class Index, class Id, class IdIndexPairPointerList,
         class IndexCompare = std::less<Index>,
         template<class, class> class IndexPointerListF = std::vector,
         template<class> class AllocatorF = std::allocator>
constexpr void callAndTrack2(Function&& f,
                             IdIndexPairPointerList& id_index_pair_pointers,
                             Id id_1, Id id_2,
                             IndexCompare const& index_compare) {
  using IdIndexPairPointer = typename IdIndexPairPointerList::value_type;
  using Index = decltype(std::declval<IdIndexPairPointer>()->second);
  using IndexPointer =
      typename std::pointer_traits<IdIndexPairPointer>::template rebind<Index>;
  using IndexPointerTraits = typename std::pointer_traits<IndexPointer>;
  using IndexPointerAllocator = AllocatorF<IndexPointer>;
  using IndexPointerList = IndexListF<IndexPointer, IndexAllocator>;

  IndexPointerList index_pointers_1;
  IndexPointerList index_pointers_2;

  for (auto& id_index_pair_pointer : id_index_pair_pointers) {
    if (*id_index_pair_pointer->first == id_1) {
      index_pointers_1.push_back(
          IndexPointerTraits::pointer_to(id_index_pair_pointer->second));
    } else if (*id_index_pair_pointer->first == id_2) {
      index_pointers_2.push_back(
          IndexPointerTraits::pointer_to(id_index_pair_pointer->second));
    }
  }

  auto index_pointer_compare = [&index_compare](IndexPointer const& a,
                                                IndexPointer const& b) {
    return index_compare(*a, *b);
  };
  std::sort(index_pointers_1.begin(), index_pointers_1.end(),
            index_pointer_compare);
  std::sort(index_pointers_2.begin(), index_pointers_2.end(),
            index_pointer_compare);

  callAndTrack2(std::forward<Function>(f), index_pointers_1.begin(),
                index_pointers_1.end(),
                funnel::layer::merger::TrackedIndices<Index, 0>{},
                index_pointers_2.begin(), index_pointers_2.end(),
                funnel::layer::merger::TrackedIndices<Index, 0>{});
}

}  // namespace tracker

}  // namespace funnel::layer_array

