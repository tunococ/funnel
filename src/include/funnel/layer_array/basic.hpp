#pragma once

#include "../layer/concepts.hpp"

#include <iterator>
#include <memory>
#include <type_traits>

namespace funnel::layer_array {

template<class LayerT, class AllocatorT, template<class, class> class BaseF>
FUNNEL_REQUIRES(is_layer<LayerT>)
class BasicLayerArray : public BaseF<LayerT, AllocatorT> {
public:
  using This = BasicLayerArray<LayerT, AllocatorT, BaseF>;
  using Super = BaseF<LayerT, AllocatorT>;
  using Layer = LayerT;
  using Allocator = AllocatorT;

  using Value = typename Layer::Value;
  using Index = typename Layer::Index;

  using LayerIterator = typename Super::iterator;
  using size_type = typename Super::size_type;
  using Id = size_type;

  constexpr BasicLayerArray(Allocator const& allocator = Allocator()) noexcept(
      std::is_nothrow_constructible_v<Super, Allocator const&>)
      : Super(allocator) {}

  constexpr BasicLayerArray(This const&) = default;
  constexpr BasicLayerArray(This&&) = default;
  constexpr This& operator=(This const&) = default;
  constexpr This& operator=(This&&) = default;

  constexpr void swap(This& other) noexcept(
      std::is_nothrow_swappable_v<Super>) {
    Super::swap(other);
  }

  constexpr void clear() { Super::clear(); }

  constexpr Allocator getAllocator() const noexcept {
    return Super::get_allocator();
  }

  constexpr size_type numLayers() const noexcept { return Super::size(); }

  constexpr Layer& layer(Id id) noexcept { return Super::operator[](id); }

  constexpr Layer& newLayer() { return Super::emplace_back(getAllocator()); }

  template<class... Args>
  constexpr Layer& newLayer(Args&&... args) {
    return Super::emplace_back(std::forward<Args>(args)...);
  }

  constexpr void deleteLayer(Id id) {
    Super::erase(std::next(Super::begin(), id));
  }

  template<class ValueLocation>
  constexpr void ownValueLocation(ValueLocation& value_location) {
    value_location.reset(this);
  }

  // For testing.
  friend class BasicLayerArrayProbe;
};

}  // namespace funnel::layer_array
