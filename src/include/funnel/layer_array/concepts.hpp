#pragma once

#include "../concepts.hpp"
#include "../layer/concepts.hpp"

#if FUNNEL_CONCEPTS

namespace funnel {

/** @brief
 *  Concept of classes that map indices to layers.
 */
template<class LayerArray>
concept is_layer_array = requires(LayerArray layer_array,
                                  typename LayerArray::Id id,
                                  typename LayerArray::Layer layer) {
  requires std::integral<typename LayerArray::Id>;
  requires is_layer<typename LayerArray::Layer>;
  { layer_array.numLayers() } -> std::convertible_to<typename LayerArray::Id>;
  { layer_array.empty() } -> std::convertible_to<bool>;
  layer_array.clear();
  layer_array.swap(layer_array);
  {
    layer_array.layer(id)
    } -> std::convertible_to<
        std::add_lvalue_reference_t<typename LayerArray::Layer>>;
  {
    layer_array.newLayer()
    } -> std::convertible_to<
        std::add_lvalue_reference_t<typename LayerArray::Layer>>;
  {
    layer_array.newLayer(std::move(layer))
    } -> std::convertible_to<
        std::add_lvalue_reference_t<typename LayerArray::Layer>>;
  layer_array.deleteLayer(id);
  {
    layer_array.getAllocator()
    } -> std::convertible_to<typename LayerArray::Allocator>;
};

}  // namespace funnel

#endif
