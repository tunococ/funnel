#pragma once

#include "../layer/concepts.hpp"

#include <cassert>
#include <iterator>
#include <memory>
#include <type_traits>
#include <vector>

namespace funnel::layer_array {

template<class LayerT, class AllocatorT, template<class, class> class BaseF>
FUNNEL_REQUIRES(is_layer<LayerT>)
class IndexedLayerArray : public BaseF<LayerT, AllocatorT> {
public:
  using This = IndexedLayerArray<LayerT, AllocatorT, BaseF>;
  using Super = BaseF<LayerT, AllocatorT>;

  using Layer = LayerT;
  using Value = typename Layer::Value;
  using Index = typename Layer::Index;

  using Allocator = typename Super::allocator_type;
  using LayerIterator = typename Super::iterator;
  using LayerIteratorAllocator = typename std::allocator_traits<
      Allocator>::template rebind_alloc<LayerIterator>;
  using LayerIterators = std::vector<LayerIterator, LayerIteratorAllocator>;

  using size_type = typename LayerIterators::size_type;
  using Id = size_type;

  constexpr IndexedLayerArray(
      Allocator const& allocator =
          Allocator()) noexcept(std::
                                    is_nothrow_constructible_v<
                                        Super, Allocator const&>&&
                                        std::is_nothrow_constructible_v<
                                            LayerIterators,
                                            LayerIteratorAllocator const&>)
      : Super(allocator), layer_iterators_(allocator) {}

  constexpr IndexedLayerArray(This const& other) noexcept(
      std::is_nothrow_copy_constructible_v<Super>&&
          std::is_nothrow_constructible_v<LayerIterators,
                                          LayerIteratorAllocator const&>)
      : Super(other), layer_iterators_(numLayers(), getAllocator()) {
    fillLayerIterators();
  }

  constexpr IndexedLayerArray(This&& other) noexcept(
      std::is_nothrow_move_constructible_v<Super>&&
          std::is_nothrow_constructible_v<LayerIterators,
                                          LayerIteratorAllocator const&>)
      : Super(std::move(other)), layer_iterators_(numLayers(), getAllocator()) {
    other.clear();
    fillLayerIterators();
  }

  constexpr This& operator=(This const& other) noexcept(
      std::is_nothrow_copy_assignable_v<Super>&&
          std::is_nothrow_copy_assignable_v<LayerIterators>) {
    Super::operator=(other);
    layer_iterators_ = other.layer_iterators_;
    fillLayerIterators();
    return *this;
  }

  constexpr This& operator=(This&& other) noexcept(
      std::is_nothrow_move_assignable_v<Super>&&
          std::is_nothrow_move_assignable_v<LayerIterators>) {
    Super::operator=(std::move(other));
    layer_iterators_ = std::move(other.layer_iterators_);
    other.clear();
    fillLayerIterators();
    return *this;
  }

  constexpr void swap(This& other) noexcept(
      std::is_nothrow_swappable_v<Super>&&
          std::is_nothrow_swappable_v<LayerIterators>) {
    Super::swap(other);
    layer_iterators_.swap(other.layer_iterators_);
  }

  constexpr void clear() {
    Super::clear();
    layer_iterators_.clear();
  }

  constexpr Allocator getAllocator() const noexcept {
    return Super::get_allocator();
  }

  constexpr size_type numLayers() const noexcept { return Super::size(); }

  constexpr Layer& layer(Id id) noexcept { return *layer_iterators_[id]; }

  constexpr Layer& newLayer() {
    Super::emplace_back(getAllocator());
    layer_iterators_.emplace_back(std::prev(Super::end()));
    return Super::back();
  }

  template<class... Args>
  constexpr Layer& newLayer(Args&&... args) {
    Super::emplace_back(std::forward<Args>(args)...);
    layer_iterators_.emplace_back(std::prev(Super::end()));
    return Super::back();
  }

  constexpr void deleteLayer(Id id) {
    layer_iterators_.erase(std::next(layer_iterators_.begin(), id));
    Super::erase(std::next(Super::begin(), id));
  }

  template<class ValueLocation>
  constexpr void ownValueLocation(ValueLocation& value_location) {
    value_location.reset(this);
  }

protected:
  LayerIterators layer_iterators_;

  const void fillLayerIterators() {
    assert((numLayers() == layer_iterators_.size()) &&
           "the size of layer_iterators_ does not match the size of the "
           "underlying collection");
    LayerIterator it{Super::begin()};
    for (Id id{0}; id < numLayers(); ++id, ++it) {
      assert((it != Super::end()) && "past-the-end iterator unexpected");
      layer_iterators_[id] = it;
    }
    assert((it == Super::end()) && "past-the-end iterator expected");
  }

  // For testing.
  friend class IndexedLayerArrayProbe;
};

}  // namespace funnel::layer_array
