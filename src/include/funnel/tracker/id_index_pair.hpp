#pragma once

#include "../layer_array/concepts.hpp"
#include "index.hpp"

#include <memory>
#include <vector>

namespace funnel::tracker {

template<class LayerArray>
using IdIndexPair =
    std::pair<typename LayerArray::Id, typename LayerArray::Index>;

template<class Function, class Id, class IdIndexPairPointerList,
         class IndexCompare = std::less<Index>,
         template<class, class> class IndexPointerListF = std::vector,
         template<class> class AllocatorF = std::allocator>
constexpr void callAndTrack(
    Function&& f, IndexCompare const& index_compare,
    IdIndexPairPointerList& id_index_pair_pointers,
    Id tracked_id,
    std::unique_ptr<TrackedIndices<Index, N>>&& tracked_indices,
    Args&&... args) {
  using IdIndexPairPointer = typename IdIndexPairPointerList::value_type;
}

}  // namespace funnel::tracker

