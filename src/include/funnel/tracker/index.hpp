#pragma once

#include <cstddef>
#include <memory>

namespace funnel::tracker {

/** @brief
 *  List of references to `Index` objects. This is meant to be used for
 *  tracking indices of elements during an operation that moves elements in a
 *  layer.
 *
 *  This struct represents a linked-list node where the *tail* element of
 *  `TrackedIndices<Index, N>` has type
 *  `std::unique<TrackedIndices<Index, N - 1>>`.
 *
 *  @tparam Index Type of indices to track.
 *  @tparam N Length of the list.
 */
template<class Index, std::size_t N>
struct TrackedIndices;

/** @brief
 *  Specialization of `TrackedIndices<IndexT, N>` that represents an empty
 *  list. This class has no data members.
 *
 *  @tparam IndexT Type of indices to track.
 */
template<class IndexT>
struct TrackedIndices<IndexT, 0> {
  /// `IndexT`.
  using Index = IndexT;

  /** @brief
   *  Does nothing.
   */
  template<class... X>
  constexpr TrackedIndices(X&&...) noexcept {}
};

/** @brief
 *  Specialization of `TrackedIndices<IndexT, N>` that represents the last
 *  element of the list of indices.
 *
 *  Since `tail` does not have any data members and will never be dereferenced,
 *  it can be declared `static` and set to `nullptr`.
 *
 *  @tparam IndexT Type of indices to track.
 */
template<class IndexT>
struct TrackedIndices<IndexT, 1> {
  /// `IndexT`.
  using Index = IndexT;

  /// `std::unique_ptr<TrackedIndices<Index, 0>>`.
  using Tail = std::unique_ptr<TrackedIndices<Index, 0>>;

  /// Reference to an `Index` to track.
  Index& head;

  /** @brief
   *  Returns the null pointer for type `TrackedIndices<Index, 0>`.
   *
   *  The purpose of this function is to provide an interface to retrieve a raw
   *  pointer inside `tail` that is consistent across all
   *  `TrackedIndices<Index, N>`.
   */
  constexpr typename Tail::pointer tailPointer() noexcept { return nullptr; }

  /** @brief
   *  Sets `head` to `index`.
   *
   *  @param index Reference to `Index`.
   */
  template<class X>
  constexpr TrackedIndices(Index& index, X) noexcept : head{index} {}
};

/** @brief
 *  General (fallback) implementation of `TrackedIndices<Index, N>`.
 *
 *  @tparam IndexT Type of indices to track.
 *  @tparam N Length of the list.
 */
template<class IndexT, std::size_t N>
struct TrackedIndices {
  /// `IndexT`.
  using Index = IndexT;

  /// `std::unique_ptr<TrackedIndices<Index, N - 1>>`.
  using Tail = std::unique_ptr<TrackedIndices<Index, N - 1>>;

  /// Reference to an `Index` to track.
  Index& head;

  /// Pointer to `Tail`, which contains `N - 1` more indices to track.
  Tail tail;

  /** @brief
   *  Returns the pointer stored in `tail`.
   *
   *  The purpose of this function is to provide an interface to retrieve a raw
   *  pointer inside `tail` that is consistent across all
   *  `TrackedIndices<Index, N>`.
   */
  constexpr typename Tail::pointer tailPointer() noexcept { return tail.get(); }

  /** @brief
   *  Constructs
   */
  constexpr TrackedIndices(Index& index, Tail&& tail) noexcept
      : head{index}, tail{std::move(tail)} {}
};

/** @brief
 *  Returns `nullptr`.
 *
 *  This is the base case for `trackIndices(...)`.
 */
constexpr std::nullptr_t trackIndices() noexcept { return nullptr; }

/** @brief
 *  Creates `TrackedIndices<Index, N>` that represents a list of indices passed
 *  as arguments to this function; `N` is the number of arguments.
 *
 *  @param index First index to track.
 *  @param indices Remaining indices to track.
 */
template<class Index, class... Indices>
constexpr std::unique_ptr<TrackedIndices<Index, sizeof...(Indices) + 1>>
trackIndices(Index& index, Indices&... indices) {
  return std::make_unique<TrackedIndices<Index, sizeof...(Indices) + 1>>(
      index, trackIndices(indices...));
}

/** @brief
 *  Calls `std::make_unique<TrackedIndices<Index, N + 1>>`.
 */
template<class Index, std::size_t N>
constexpr std::unique_ptr<TrackedIndices<Index, N + 1>> makeTrackedIndices(
    Index& head, std::unique_ptr<TrackedIndices<Index, N>>&& tail) {
  return std::make_unique<TrackedIndices<Index, N + 1>>(head, std::move(tail));
}

/** @brief
 *  Returns `p.get()`.
 *
 *  The purpose of this function is to provide a consistent interface for
 *  retrieving a raw pointer from different pointer types.
 *
 *  @param p Unique pointer of type `T`.
 */
template<class T, class Deleter>
constexpr typename std::unique_ptr<T, Deleter>::pointer rawPointer(
    std::unique_ptr<T, Deleter> const& p) noexcept {
  return p.get();
}

/** @brief
 *  Returns `p`.
 *
 *  The purpose of this function is to provide a consistent interface for
 *  retrieving a raw pointer from different pointer types.
 *
 *  @param p Raw pointer of type `T`.
 */
template<class T>
constexpr T* rawPointer(T* p) noexcept {
  return p;
}

/** @brief
 *  Returns `nullptr`.
 *
 *  The purpose of this function is to provide a consistent interface for
 *  retrieving a raw pointer from different pointer types.
 */
constexpr std::nullptr_t rawPointer(std::nullptr_t) noexcept { return nullptr; }

}  // namespace funnel::tracker

